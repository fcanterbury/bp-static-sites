<?php
/**
 * Main Administrator Component File
 * This defines what controller to use and what task to execute.
 *
 * @package     DB Replacer
 * @version     1.2.0
 *
 * @author      Peter van Westen <peter@nonumber.nl>
 * @link        http://www.nonumber.nl
 * @copyright   Copyright © 2011 NoNumber! All Rights Reserved
 * @license     http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// No direct access
defined( '_JEXEC' ) or die();

$lang =& JFactory::getLanguage();
if ( $lang->getTag() != 'en-GB' ) {
	// Loads English language file as fallback (for undefined stuff in other language file)
	$lang->load( 'com_dbreplacer', JPATH_ADMINISTRATOR, 'en-GB' );
}
$lang->load( 'com_dbreplacer', JPATH_ADMINISTRATOR, null, 1 );

jimport( 'joomla.filesystem.file' );
$mainframe =& JFactory::getApplication();

// return if NoNumber! Framework plugin is not installed
if ( !JFile::exists( JPATH_PLUGINS.DS.'system'.DS.'nnframework'.DS.'nnframework.php' ) ) {
	$mainframe->set( '_messageQueue', '' );
	$mainframe->enqueueMessage( JText::_( 'DBR_NONUMBER_FRAMEWORK_PLUGIN_NOT_INSTALLED' ), 'error' );
	return;
}

// give notice if NoNumber! Framework plugin is not enabled
$nnep = JPluginHelper::getPlugin( 'system', 'nnframework' );
if ( !isset( $nnep->name ) ) {
	$mainframe->set( '_messageQueue', '' );
	$mainframe->enqueueMessage( JText::_( 'DBR_NONUMBER_FRAMEWORK_PLUGIN_NOT_ENABLED' ), 'notice' );
	return;
}

// load the NoNumber! Framework language file
if ( $lang->getTag() != 'en-GB' ) {
	// Loads English language file as fallback (for undefined stuff in other language file)
	$lang->load( 'plg_system_nnframework', JPATH_ADMINISTRATOR, 'en-GB' );
}
$lang->load( 'plg_system_nnframework', JPATH_ADMINISTRATOR, null, 1 );

// Version check
require_once JPATH_PLUGINS.DS.'system'.DS.'nnframework'.DS.'helpers'.DS.'versions.php';
$versions = NNVersions::instance();
$version = '';
$xml = JApplicationHelper::parseXMLInstallFile( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_dbreplacer'.DS.'dbreplacer.xml' );
if ( $xml && isset( $xml['version'] ) ) {
	$version = $xml['version'];
}
$versions->setMessage( $version, 'version_dbreplacer', 'http://www.nonumber.nl/versions', 'http://www.nonumber.nl/dbreplacer/download' );

// If no controller then default controller = 'default'
$controller = JRequest::getCmd( 'controller', 'default' );

// Set the controller page
require_once JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';

// Create the controller dbreplacerController
$classname = 'DBReplacerController'.$controller;

// Create a new class of classname and set the default task: display
$controller = new $classname( array( 'default_task'=>'display' ) );

// Perform the Request task
$controller->execute( JRequest::getCmd( 'task' ) );

// Redirect if set by the controller
$controller->redirect();

// Place Commercial License Code check
require_once JPATH_PLUGINS.DS.'system'.DS.'nnframework'.DS.'helpers'.DS.'licenses.php';
$licenses = NNLicenses::instance();
echo $licenses->getMessage( 'DB Replacer' );

echo '<p style="text-align:center;">'.JText::_( 'DB_REPLACER' );
if ( $version ) {
	echo ' v'.$version;
}
echo ' - '.JText::_( 'COPYRIGHT' ).' (C) 2011 NoNumber! '.JText::_( 'ALL_RIGHTS_RESERVED' ).'</p>';