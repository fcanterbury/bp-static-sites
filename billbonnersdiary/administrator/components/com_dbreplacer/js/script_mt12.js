/**
 * Main JavaScript file (MooTools 1.2 compatible)
 *
 * @package     DB Replacer
 * @version     1.3.2
 *
 * @author      Peter van Westen <peter@nonumber.nl>
 * @link        http://www.nonumber.nl
 * @copyright   Copyright © 2011 NoNumber! All Rights Reserved
 * @license     http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

window.addEvent( 'domready', function() {
	if ( !DBR ) {
		DBR = new DBR_class();
	}
});

// prevent init from running more than once
if ( typeof( window['DBR'] ) == "undefined" ) {
	var DBR = null;
}

var DBR_class = new Class({
	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	overlay: null, // holds all the overlay object
	params: {}, // holds the form values
	update: {}, // holds data on what fields to update
	pending: 1, // holds data on what fields to update

	initialize: function()
	{
		var self = this;
		var client = this._getClient();

		this.fnc = ( function(){
			self.pending++;
			( function(){
				self.pending--;
				if ( self.pending < 1 ) {
					self.updateFields();
				}
			} ).delay( 500 );
		} );

		this.overlay = new Element( 'div', {
			id: 'DBR_overlay',
			styles: {
				backgroundColor: 'black',
				position: 'fixed',
				left: 0,
				top: 0,
				width: '100%',
				height: '100%',
				zIndex: 5000
			}
		} ).addEvent( 'click', function(){ self._finishLoad(); } );

		if ( client.isIE && !client.isIE7 ) {
			this.overlay.style.position = 'absolute';
			this.overlay.style.height = this._getDocHeight() + 'px';
			this._fixTop();
			window.addEvent( 'scroll', function(){ self._fixTop(); } );
		}

		this.overlay.fade( 'hide' );
		document.id(document.body).adopt( this.overlay );

		this.submit = document.id( 'dbr_submit' );
		//this.submit.addEvent( 'click', function(){ self.protectSpaces(); } );
		this.submit.fade( 'hide' );
	},

	getXML : function( field, params )
	{
		if ( !field ) {
			field = 'tables';
		}
		var self = this;
		this._startLoad();
		var myXHR = new Request( {
			method: 'post',
			url: 'index.php',
			onSuccess: function( data ) {self._insertData( data, field );},
			onFailure: function() {self._finishLoad();}
		} ).send( 'nn_qp=1&folder=administrator.components.com_dbreplacer&file=dbreplacer.inc.php&field='+field+params );
	},

	resetFields : function( forced )
	{
		var elems = document.getElements( '.dbr_element' );
		elems.each( function( el ) {
			el.value = '';
		} );
		this.updateFields( 1 );
	},

	protectSpaces: function() {
		document.getElements( '.dbr_element' ).each( function( el ) {
			if ( el.type == 'textarea' ) {
				el.value = el.value.replace( /^ /, '||space||' ).replace( / $/, '||space||' );
			}
		});
	},

	updateFields : function( forced )
	{
		var self = this;
		var elems = document.getElements( '.dbr_element' );
		var update = 0;

		var updateall = 0;

		elems.each( function( el ) {
			if ( el.name == 'tables' && el.type != 'select-one' ) {
				updateall = 1;
			}
			switch ( el.type ) {
				case 'checkbox':
					val = ( el.checked ) ? el.value : '';
					break;
				case 'radio':
				case 'select-multiple':
					val = self._multipleSelectValues( el );
					break;
				default:
					val = el.value;
					break;
			}
			elname = el.name.replace( '[]', '' );
			if ( self.params[elname] != val ) {
				self.update[elname] = 1;
				update = 1;
			} else {
				self.update[elname] = 0;
			}
			self.params[elname] = val;
		} );

		if ( forced || updateall ) {
			self._updateField( 'tables', forced );
			self._updateField( 'columns', forced );
			self._updateField( 'rows', forced );
			update = 0;
		}
		if ( update ) {
			if ( this.update.tables ) {
				self._updateField( 'columns', 1 );
			}
			self._updateField( 'rows', 1 );
		}
		self.pending = 0;
	},

	_updateField : function( type, clear )
	{
		if ( clear ) {
			this.params[type] = '';
		}
		var params = '';
		for ( key in this.params ) {
			params += '&params['+key+']='+encodeURIComponent( this.params[key] );
		}
		this.getXML( type, params );
	},

	_updateActions : function()
	{
		var self = this;
		var elems = document.getElements( '.dbr_element' );
		elems.each( function( el ) {
			switch ( el.type ) {
				case 'radio':
				case 'checkbox':
					el.addEvent( 'click', self.fnc );
					el.addEvent( 'keyup', self.fnc );
					break;
				case 'select':
				case 'select-one':
				case 'select-multiple':
				case 'text':
				case 'textarea':
					el.addEvent( 'change', self.fnc );
					el.addEvent( 'keyup', self.fnc );
					break;
				default:
					el.addEvent( 'change', self.fnc );
					break;
			}
		} );
	},

	_removeActions : function()
	{
		var self = this;
		var elems = document.getElements( '.dbr_element' );
		elems.each( function( el ) {
			switch ( el.type ) {
				case 'radio':
				case 'checkbox':
					el.removeEvent( 'click', self.fnc );
					el.removeEvent( 'keyup', self.fnc );
					break;
				case 'select':
				case 'select-one':
				case 'text':
					el.removeEvent( 'change', self.fnc );
					el.removeEvent( 'keyup', self.fnc );
					break;
				default:
					el.removeEvent( 'change', self.fnc );
					break;
			}
		} );
	},

	_startLoad : function()
	{
		this.overlay.setStyle( 'cursor', 'wait' );
		this.overlay.fade( 0.2 );
	},

	_finishLoad : function()
	{
		this._updateActions();
		var self = this;
		( function() {self.overlay.setStyle( 'cursor', '' );} ).delay( 200 ) ;
		this.overlay.fade( 'out' );
	},

	_insertData : function( data, field )
	{
		var el = document.id( 'dbr_'+field )
		if ( el ) {
			el.set( 'html', data );
		}
		if ( field == 'rows' ) {
			if ( !( data.indexOf( '<span class="replace_string">' ) === -1 ) ) {
				this.submit.fade( 'in' );
			} else {
				this.submit.fade( 'out' );
			}
		}
		this._finishLoad();
	},

	_getClient : function()
	{
		var ua = navigator.userAgent.toLowerCase();
		var client = {
			isStrict: document.compatMode == "CSS1Compat",
			isOpera: ua.indexOf("opera") > -1,
			isIE: ua.indexOf("msie") > -1,
			isIE7: ua.indexOf("msie 7") > -1,
			isSafari: /webkit|khtml/.test(ua),
			isWindows: ua.indexOf("windows") != -1 || ua.indexOf("win32") != -1,
			isMac: ua.indexOf("macintosh") != -1 || ua.indexOf("mac os x") != -1,
			isLinux: ua.indexOf("linux") != -1
		};
		return client;
	},

	_getDocHeight : function()
	{
		var client = this._getClient();
		var h = window.innerHeight;
		var mode = document.compatMode;
		if ((mode || client.isIE) && !client.isOpera) {
			h = client.isStrict ? document.documentElement.clientHeight: document.body.clientHeight
		}
		return h;
	},

	_fixTop : function()
	{
		this.overlay.style.top = document.documentElement.scrollTop + 'px';
	},

	_multipleSelectValues: function( el )
	{
		var vals = new Array();
		for( j = 0; j < el.options.length; j++ ) {
			if( el.options[j].selected) {
				vals[vals.length] = el.options[j].value;
			}
		}
		return vals.join(',');
	}
});