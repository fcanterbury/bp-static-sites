<?php
/**
 * DB Replacer Default View Template
 *
 * @package         DB Replacer
 * @version         3.0.8
 *
 * @author          Peter van Westen <peter@nonumber.nl>
 * @link            http://www.nonumber.nl
 * @copyright       Copyright © 2012 NoNumber All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die;

// Import html tooltips
JHtml::_('behavior.mootools');
JHtml::_('behavior.tooltip');

/* SCRIPTS */
$alert = "DBR.protectSpaces();form.task.value = 'replace';form.submit();";
if ($this->config->show_alert) {
	$alert = "if ( confirm( '" . str_replace(array('<br />', "\n", "'"), array('\n','\n', "\\'"), JText::_('DBR_ARE_YOU_REALLY_SURE')) . "' ) ) {" . $alert . "}";
}
$alert = "if ( confirm( '" . str_replace(array('<br />', "\n", "'"), array('\n','\n', "\\'"), JText::_('NN_ARE_YOU_SURE')) . "' ) ) {" . $alert . "}";
$script = "
	function submitform( task )
	{
		var form = document.adminForm;
		try {
			form.onsubmit();
			}
		catch( e ) {}
		" . $alert . "
	}
	var DBR_root = '" . JURI::root() . "';
	window.addEvent( 'domready', function() {
		DBR.updateFields();
	});
";
JFactory::getDocument()->addScriptDeclaration($script);
JHtml::script('dbreplacer/script.min.js', false, true);
JHtml::script('nnframework/script.min.js', false, true);
JHtml::script('nnframework/toggler.min.js', false, true);

// Version check
require_once JPATH_PLUGINS . '/system/nnframework/helpers/versions.php';
echo NNVersions::getInstance()->getMessage('dbreplacer', '', '', 'component');

$s = JRequest::getVar('search', '', 'default', 'none', 2);
$r = JRequest::getVar('replace', '', 'default', 'none', 2);
$s = str_replace('||space||', ' ', $s);
$r = str_replace('||space||', ' ', $r);
?>
	<form action="<?php echo $this->request_url; ?>" method="post" name="adminForm" id="adminForm">
		<input type="hidden" name="controller" value="default" />
		<input type="hidden" name="task" value="" />

		<table style="width:100%;">
			<tr>
				<td style="width:25%;">
					<div class="col dbr_select dbr_tables">
						<fieldset class="adminform">
							<legend><?php echo html_entity_decode(JText::_('DBR_TABLES')); ?></legend>
							<div id="dbr_tables"><?php echo $this->tables; ?></div>
						</fieldset>
					</div>
				</td>
				<td style="width:25%;">
					<div class="col dbr_select dbr_columns">
						<fieldset class="adminform">
							<legend><?php echo html_entity_decode(JText::_('DBR_COLUMNS')); ?></legend>
							<div id="dbr_columns">
								<input type="hidden" name="columns"
									value="<?php echo implode(',', JFactory::getApplication()->input->get('columns', array(0), 'array')); ?>"
									class="dbr_element" />
							</div>
						</fieldset>
					</div>
				</td>
				<td>
					<div class="col dbr_select dbr_where">
						<fieldset class="adminform">
							<legend><?php echo html_entity_decode(JText::_('DBR_WHERE')); ?></legend>
							<?php echo JText::_('DBR_WHERE_DESC'); ?><br />
							<textarea name="where" class="dbr_element" cols="30"
								rows="3"><?php echo JRequest::getVar('where', '', 'default', 'none', 2); ?></textarea>
						</fieldset>
					</div>

					<div style="clear:both;"></div>

					<div class="col dbr_select dbr_search">
						<fieldset class="adminform">
							<legend><?php echo html_entity_decode(JText::_('DBR_SEARCH')); ?></legend>
							<div style="clear:both;margin-bottom: 5px;">
								* = <?php echo JText::_('DBR_ALL'); ?> &nbsp; &nbsp;
								NULL = <?php echo JText::_('DBR_NULL'); ?>
							</div>

							<textarea name="search" class="dbr_element" cols="30" rows="3"><?php echo $s; ?></textarea>

							<div style="clear:both;margin-top: 5px;">
								<div style="float:left;">
									<input type="checkbox" value="1" name="case" id="dbr_case"
										class="dbr_element" <?php  echo JFactory::getApplication()->input->getInt('case') ? 'checked="checked"' : ''; ?>>
									<label for="dbr_case"
										style="clear: right;min-width: 0;"><?php echo JText::_('DBR_CASE_SENSITIVE'); ?></label>
									<input type="checkbox" value="1" name="regex" id="dbr_regex"
										class="dbr_element" <?php echo JFactory::getApplication()->input->getInt('regex') ? 'checked="checked"' : ''; ?>>
									<label for="dbr_regex"
										style="clear: right;min-width: 0;"><?php echo JText::_('DBR_REGULAR_EXPRESSION'); ?></label>
								</div>
								<div style="float:left;">
									<div id="<?php echo rand(1000000, 9999999); ?>___regex.1" class="nntoggler">
										<input type="checkbox" value="1" name="utf8" id="dbr_utf8"
											class="dbr_element" <?php echo JFactory::getApplication()->input->getInt('utf8') ? 'checked="checked"' : ''; ?>>
										<label for="dbr_utf8"
											style="clear: right;min-width: 0;"><?php echo JText::_('NN_UTF8'); ?></label>
									</div>
								</div>
							</div>
						</fieldset>
					</div>

					<div style="clear:both;"></div>

					<div class="col dbr_select dbr_replace">
						<fieldset class="adminform">
							<legend><?php echo html_entity_decode(JText::_('DBR_REPLACE')); ?></legend>
							<textarea name="replace" class="dbr_element" cols="30" rows="3"><?php echo $r; ?></textarea>

							<div style="clear:both;margin-top: 5px;">
								<div class="button2-left" style="margin-bottom: 10px;">
									<div class="blank">
										<a onclick="DBR.resetFields();"><?php echo JText::_('Reset'); ?></a>
									</div>
								</div>
								<div id="dbr_submit">
									<div class="button1">
										<div class="next">
											<a onclick="submitform();"><?php echo JText::_('DBR_REPLACE'); ?></a>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
				</td>
			</tr>
		</table>

		<div style="clear:both;"></div>

		<div class="col dbr_select">
			<fieldset class="adminform">
				<legend><?php echo html_entity_decode(JText::_('DBR_PREVIEW')); ?></legend>
				<div id="dbr_rows"></div>
			</fieldset>
		</div>

		<div style="clear:both;"></div>
	</form>

<?php

// Copyright
echo NNVersions::getInstance()->getCopyright('DB_REPLACER', '', 13004, 'dbreplacer', 'component');
