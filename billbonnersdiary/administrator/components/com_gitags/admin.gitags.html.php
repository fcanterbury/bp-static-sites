<?php
/**
* @package	Joomla
* @subpackage	GiTags
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license	GNU/GPL, see LICENSE.php
* based on JTags : http://extensions.joomla.org/extensions/search-a-indexing/tags-a-clouds/5728 developed by Jacek Zielinski
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

/* Check to ensure this file is included in Joomla! */
defined( '_JEXEC' ) or die( 'Restricted access' );


class HTML_gitags
{
	function showTags( $option, &$rows, &$pageNav )
	{
		$mainframe = JFactory::getApplication();
	  ?>
	  <form action="<?php echo JRoute::_('index.php?option=com_gitags'); ?>" method="post" name="adminForm" id="adminForm">
	  <table class="adminlist">
	    <thead>
	      <tr>
	        <th width="20">
	          <input type="checkbox" name="toggle"
	               value="" onclick="checkAll(<?php echo
	               count( $rows ); ?>);" />
	        </th>
	        <th class="title"><?php echo JText::_('Name'); ?></th>
	        <th width="10%"><?php echo JText::_('Count'); ?></th>
	      </tr>
	    </thead>
	    <?php
		jimport('joomla.filter.output');
	    $k = 0;
            
	    $offset = $mainframe->getCfg('offset');
	    for ($i=0, $n=count( $rows ); $i < $n; $i++)
	    {
			$row = &$rows[$i];
			if ($row->publish_down != '0000-00-00 00:00:00' && (time() > strtotime($row->publish_down)+( $offset * 60 * 60))) continue;
			$checked = JHTML::_('grid.id', $i, $row->tag_id );
			/* $published = JHTML::_('grid.published', $row, $i ); */
			$link = JFilterOutput::ampReplace( 'index.php?option=com_gitags&task=edit&cid[]='. $row->tag_id );
	      ?>
	      <tr class="<?php echo "row$k"; ?>">
	        <td>
	          <?php echo $checked; ?>
	        </td>
	        <td>
			<a href="<?php echo $link; ?>">
	          <?php echo $row->name; ?></a>
	        </td>
	        <td align="center">
	          <?php echo $row->used; ?>
	        </td>
	      </tr>
	      <?php
	      $k = 1 - $k;
	    }
	    ?>
		<tfoot>
		 <td colspan="7"><?php echo $pageNav->getListFooter(); ?></td>
		</tfoot>
	  </table>
          <input type="hidden" name="task" value="" />
          <input type="hidden" name="boxchecked" value="0" />
	  <?php echo JHtml::_('form.token'); ?>
	  </form>
	  <?php
	}

	function editTag($option, $row)
	{
		?>
		<form action="<?php echo JRoute::_('index.php?option=com_gitags'); ?>" method="post" name="adminForm"  id="adminForm">
	  	<table class="adminlist">
		  	<thead>
		  		<tr>
		  			<th>
		  			<?php echo JText::_('Tag Name'); ?>:1
		  			</th>
		  			<th>
		  			<input type="text" name="tagName" id="tagName" size="50" value="<?php echo $row->name; ?>"/>
		  			</th>
		  		</tr>
		  	</thead>
	  	</table>
	  	<input type="hidden" name="tag_id" value="<?php echo $row->tag_id; ?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
	  	<?php echo JHtml::_('form.token'); ?>
                </form>
		<?php
	}
        
       function importTags( $result )
	{   
           
           ?>
                <form action="<?php echo JRoute::_('index.php?option=com_gitags'); ?>" method="post" name="adminForm"  id="adminForm">
                    <?php echo $result ?>
                    <input type="hidden" name="task" value="" />
                    <input type="hidden" name="boxchecked" value="0" />
                    <?php echo JHtml::_('form.token'); ?>
                </form>
           <?php
       }
}
?>