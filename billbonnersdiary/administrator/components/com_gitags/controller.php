<?php
/**
 * @package	Joomla
 * @subpackage	GiTags
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license	GNU/GPL, see LICENSE.php
 * based on JTags : http://extensions.joomla.org/extensions/search-a-indexing/tags-a-clouds/5728 developed by Jacek Zielinski
 * 
 * Joomla! is free software. This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

/* Check to ensure this file is included in Joomla! */
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

/**
 * GiTags Component Controller
 *
 * @author		Gabriel Moise <gabi.moise@gmail.com>
 * @package		Joomla
 * @subpackage          GiTags
 * @since 1.6
 */

class GiTagsController extends JController
{
	function __construct( $default = array() )
	{
		parent::__construct( $default );
		$this->registerTask( 'add' , 'edit' );
		$this->registerTask( 'apply', 'save' );
		$this->registerTask( 'savetags', 'saveTags' );
		$this->registerTask( 'remove', 'removeTags' );
                $this->registerTask( 'import', 'importTags' );
	}
       
        function importTags()
        {
            $db =& JFactory::getDBO();
            $query = 'SELECT id, metakey
                            FROM #__content
                      WHERE id not in (select item_id from #__gitags_items)
                        ORDER BY id ASC';
            $db->setQuery( $query );
            $rows = $db->loadObjectList(); 
            
            
            if ($db->getErrorNum()) {
               
		die ( $db->stderr() );
                
            }
            $tags_inserted = 0;
            $err = '';
            foreach ($rows as $row )
            {
              $arr = explode(',',$row->metakey);
              for($i=0;$i<count($arr);$i++)
              {
                 $query = "SELECT tag_id FROM #__gitags_tags WHERE name = '".$arr[$i]."'";
                 $db->setQuery( $query );
                 $tags = $db->loadObjectList(); 
                 
                 if ( count($tags)>0 )
                 {  
                    
                     $query = " INSERT into #__gitags_items (component, tag_id,item_id) VALUES ('com_content',".$tags[0]->tag_id.",".$row->id.")";
                     $db->setQuery($query);
                     
                     
                     if (!$db->query())
                     {
                         $err .= $db->stderr() . '<br />';
                     }
                 } 
                 else 
                 {
                     $query2 = 'INSERT INTO #__gitags_tags (`name`) VALUES (\''.strtolower(trim(str_replace("'","`",$arr[$i]))).'\')';
                     $db->setQuery($query2);
                     $tags_inserted++;
                     
                     if (!$db->query())
                     {
                         die ($db->stderr());
                     }
                        
                     $query_last = 'SELECT LAST_INSERT_ID();';
                     $db->setQuery($query_last); 
                     $id = $db->loadResult();
                     
                     if ($id == 0)
                         die ($query);
                     $query = " INSERT into #__gitags_items (component, tag_id,item_id) VALUES ('com_content',".$id.",".$row->id.")";   
                     $db->setQuery($query); 
                     
                     if (!$db->query())
                     {
                         $err .= $db->stderr() . '<br />';
                     }
                 }
                
                  
              }
                
            }
            if ($err == "")
            {
                if (count($rows) == 0)
                    $err = 'No article need tags';
                else
                    $err = 'Meta succesfully imported!';
            } else {
                $err .= 'Imported with some errors!';
            }
            HTML_gitags::importTags($err );
           
            
        }
        
	/**
	* Shows tags
	*
	* @access	public
	* @since	1.6
	*/
	function showTags()
	{
		global $option, $mainframe;

		$limit = JRequest::getVar('limit', 20);
		$limitstart = JRequest::getVar('limitstart', 0);

		$db =& JFactory::getDBO();

		$query = 'SELECT SQL_CALC_FOUND_ROWS #__gitags_tags.tag_id, name, COUNT( #__gitags_tags.tag_id ) AS used, publish_down
				  FROM #__gitags_items
				  NATURAL JOIN #__gitags_tags
				  JOIN #__content ON (#__gitags_items.item_id = #__content.id)
				  WHERE state = 1 AND NOW() > publish_up
				  GROUP BY #__gitags_tags.tag_id
				  ORDER BY name ASC';
		$db->setQuery( $query, $limitstart, $limit );
		$rows = $db->loadObjectList(); 

		if ($db->getErrorNum()) {
			echo $db->stderr();
			return false;
		}
		$db->setQuery('SELECT FOUND_ROWS();');
		$total = $db->loadResult();
		
		jimport('joomla.html.pagination');

		$pageNav = new JPagination($total, $limitstart, $limit);

		HTML_gitags::showTags( $option, $rows, $pageNav );
	}

	/**
	* Edits specified tag
	*
	* @access	public
	* @since	1.6
	*/
	function edit()
	{
		global $option;
		$cid = JRequest::getVar( 'cid' , array(0) , '' , 'array' );
		$tagId = $cid[0];

		$row =& JTable::getInstance('gitags', 'Table');
		$row->load($tagId);

		HTML_gitags::editTag($option, $row);
	}

	/**
	* Saves specified tag
	*
	* @access	public
	* @since	1.6
	*/
	function save()
	{
		global $option;
		$row =& JTable::getInstance('gitags', 'Table');

		if (!$row->bind(JRequest::get('post'))) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}

		$row->name = JRequest::getVar( 'tagName', '', 'post', 'string', JREQUEST_ALLOWRAW );
		//if save to DB fails
		if (!$row->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}

		switch ($this->_task)
		{
			case 'apply':
				$msg = JText::_('Tag updated');
				$link = 'index.php?option=com_gitags&task=edit&cid[]='. $row->tag_id;
				break;

			case 'save':
			default:
				$msg = JText::_('Tag saved');
				$link = 'index.php?option=com_gitags';
				break;
		}

		$this->setRedirect($link, $msg);
	}

	/**
	* Removes all tags not linked to any article. Just to keep database clean.
	*
	* @access	public
	* @since	1.6
	*/
	function removeTags()
	{
		global $option;

		$db =& JFactory::getDBO();

		$query = 'DELETE FROM #__gitags_items
				  USING #__gitags_items
				  NATURAL JOIN #__gitags_tags
				  LEFT JOIN #__content
				  ON (#__gitags_items.item_id = #__content.id)
				  WHERE (state IS NULL)';
	    $db->setQuery( $query );
	    if (!$db->query())
	    {
	      echo "<script> alert('".$db->getErrorMsg()."');
	      window.history.go(-1); </script>\n";
	    }

		$aRows = $db->getAffectedRows();

		$query = 'DELETE FROM #__gitags_tags
				  USING #__gitags_tags
				  NATURAL LEFT JOIN #__gitags_items
				  WHERE item_id IS NULL';
	    $db->setQuery( $query );
	    if (!$db->query())
	    {
	      echo "<script> alert('".$db->getErrorMsg()."');
	      window.history.go(-1); </script>\n";
	    }

		$bRows = $db->getAffectedRows();

		$affectedRows = $aRows + $bRows;

		$link = 'index.php?option=com_gitags';
		$this->setRedirect($link, 'Useless entries ('.$affectedRows.' rows) has been successfully removed.');
	}
}

?>