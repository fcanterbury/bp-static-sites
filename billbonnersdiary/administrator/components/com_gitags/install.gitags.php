<?php
/* Check to ensure this file is included in Joomla! */
defined( '_JEXEC' ) or die( 'Restricted access' );

function com_install()
{
	?>
	<div class="header">Congratulations, GiTags installed!</div>
	<p>
	Congratulations, GiTags component is ready to work. To enable full functionality please install and enable also plugins and module.
	</p>
	<?php
}
?>