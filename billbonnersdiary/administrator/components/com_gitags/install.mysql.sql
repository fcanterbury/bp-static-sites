CREATE TABLE IF NOT EXISTS `#__gitags_items` (
  `component` varchar(30) NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`component`,`tag_id`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;
CREATE TABLE IF NOT EXISTS `#__gitags_tags` (
  `tag_id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY  (`tag_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;