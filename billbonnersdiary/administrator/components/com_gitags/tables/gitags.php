<?php
/**
* @package	Joomla
* @subpackage	Newsfeeds
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license	GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

/* Check to ensure this file is included in Joomla! */
defined('_JEXEC') or die('Restricted access');

/**
 * @author	Gabi Moise <gabi.moise@gmail.com>
 * @package	Joomla
 * @subpackage	GiTags
 * based on JTags : http://extensions.joomla.org/extensions/search-a-indexing/tags-a-clouds/5728 developed by Jacek Zielinski
*/
class TableGiTags extends JTable
{
	/** @var int tag id */
	var $tag_id = null;
	/** @var string name */
	var $name = null;

	/**
	 * @param database A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct( '#__gitags_tags', 'tag_id', $db );
	}
}

?>