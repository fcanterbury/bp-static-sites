<?php
/**
* @package		Joomla
* @subpackage	Newsfeeds
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

/* Check to ensure this file is included in Joomla! */
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * @author	Gabi Moise <gabi.moise@gmail.com>
 * @package	Joomla
 * @subpackage	GiTags
*/

class TOOLBAR_gitags {
	function _NEW() {
		JToolBarHelper::title( JText::_( 'Edit Tag' ), 'generic.png' );
		JToolBarHelper::save();
		JToolBarHelper::apply();
		JToolBarHelper::cancel();
	}

	function _DEFAULT() {
		JToolBarHelper::title( JText::_( 'Manage Tags' ), 'generic.png' );
		JToolBarHelper::custom('remove', 'delete', 'delete', 'Remove tags from deleted articles', false, false);
                JToolBarHelper::custom( 'import', 'move', 'move', 'Import', false, false );
		JToolBarHelper::editList();
	}
        
        function _IMPORT()
        {   
            JToolBarHelper::title( JText::_( 'Manage Tags' ), 'generic.png' );
            JToolBarHelper::back();
        }
}

?>
