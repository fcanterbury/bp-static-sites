<?php
/* Check to ensure this file is included in Joomla! */
defined('_JEXEC') or die('Restricted access');

function com_uninstall() {
    ?>
    <div class="header">GiTags was removed from your system.</div>
    <p>
    	To completely remove the software from your system, be sure to also un-install the plugins and module. Thanks for using it, and we hope you will come back!
    </p>
    <?php
}
?>