<?php
/**
 * NoNumber! Extension Manager Details page
 *
 * @package			NoNumber! Extension Manager
 * @version			2.6.7
 *
 * @author			Peter van Westen <peter@nonumber.nl>
 * @link			http://www.nonumber.nl
 * @copyright		Copyright © 2011 NoNumber! All Rights Reserved
 * @license			http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// No direct access
defined( '_JEXEC' ) or die();

$ext = JRequest::getCmd( 'ext' );
$types = explode( ',', JRequest::getString( 'types' ) );

jimport( 'joomla.filesystem.file' );

$file = '';
$xml = '';
$missing = '';

if ( in_array( 'com', $types ) ) {
	if ( JFile::exists( JPATH_ADMINISTRATOR.'/components/com_'.$ext.'/'.$ext.'.xml' ) ) {
		$xml = JPATH_ADMINISTRATOR.'/components/com_'.$ext.'/'.$ext.'.xml';
	} else if ( JFile::exists( JPATH_SITE.'/components/com_'.$ext.'/'.$ext.'.xml' ) ) {
		$xml = JPATH_SITE.'/components/com_'.$ext.'/'.$ext.'.xml';
	} else if ( JFile::exists( JPATH_ADMINISTRATOR.'/components/com_'.$ext.'/com_'.$ext.'.xml' ) ) {
		$xml = JPATH_ADMINISTRATOR.'/components/com_'.$ext.'/com_'.$ext.'.xml';
	} else if ( JFile::exists( JPATH_SITE.'/components/com_'.$ext.'/com_'.$ext.'.xml' ) ) {
		$xml = JPATH_SITE.'/components/com_'.$ext.'/com_'.$ext.'.xml';
	}
	if ( !$xml ) {
		$missing .= '|com';
	} else if ( !$file ) {
		$file = $xml;
	}
}

if ( in_array( 'plg_system', $types ) ) {
	if ( JFile::exists( JPATH_PLUGINS.'/system/'.$ext.'/'.$ext.'.xml' ) ) {
		$xml = JPATH_PLUGINS.'/system/'.$ext.'/'.$ext.'.xml';
	} else if ( JFile::exists( JPATH_PLUGINS.'/system/'.$ext.'.xml' ) ) {
		$xml = JPATH_PLUGINS.'/system/'.$ext.'.xml';
	}
	if ( !$xml ) {
		$missing .= '|plg_system';
	} else if ( !$file ) {
		$file = $xml;
	}
}
if ( in_array( 'plg_editors-xtd', $types ) ) {
	if ( JFile::exists( JPATH_PLUGINS.'/editors-xtd/'.$ext.'/'.$ext.'.xml' ) ) {
		$xml = JPATH_PLUGINS.'/editors-xtd/'.$ext.'/'.$ext.'.xml';
	} else if ( JFile::exists( JPATH_PLUGINS.'/editors-xtd/'.$ext.'.xml' ) ) {
		$xml = JPATH_PLUGINS.'/editors-xtd/'.$ext.'.xml';
	}
	if ( !$xml ) {
		$missing .= '|plg_editors-xtd';
	} else if ( !$file ) {
		$file = $xml;
	}
}
if ( in_array( 'mod', $types ) ) {
	if ( JFile::exists( JPATH_ADMINISTRATOR.'/modules/mod_'.$ext.'/'.$ext.'.xml' ) ) {
		$xml = JPATH_ADMINISTRATOR.'/modules/mod_'.$ext.'/'.$ext.'.xml';
	} else if ( JFile::exists( JPATH_SITE.'/modules/mod_'.$ext.'/'.$ext.'.xml' ) ) {
		$xml = JPATH_SITE.'/modules/mod_'.$ext.'/'.$ext.'.xml';
	} else if ( JFile::exists( JPATH_ADMINISTRATOR.'/modules/mod_'.$ext.'/mod_'.$ext.'.xml' ) ) {
		$xml = JPATH_ADMINISTRATOR.'/modules/mod_'.$ext.'/mod_'.$ext.'.xml';
	} else if ( JFile::exists( JPATH_SITE.'/modules/mod_'.$ext.'/mod_'.$ext.'.xml' ) ) {
		$xml = JPATH_SITE.'/modules/mod_'.$ext.'/mod_'.$ext.'.xml';
	}
	if ( !$xml ) {
		$missing .= '|mod';
	} else if ( !$file ) {
		$file = $xml;
	}
}

if ( $file ) {
	$xml = JApplicationHelper::parseXMLInstallFile( $file );
	if ( $xml && isset( $xml['version'] ) ) {
		echo $xml['version'];
		echo $missing;
	}
}
exit();