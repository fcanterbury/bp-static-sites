<?php
/**
 * Tag Meta Community component for Joomla 1.6
 *
 * @author Luigi Balzano (info@sistemistica.it)
 * @package TagMeta
 * @copyright Copyright 2009 - 2011
 * @license GNU Public License
 * @link http://www.sistemistica.it
 * @version 1.6.0
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controllerform');

/**
 * Tag Meta Controller Synonym
 *
 * @package TagMeta
 *
 */
class TagMetaControllerSynonym extends JControllerForm
{
}