<?php

/**
 *
 * @author          Gabriel Moise <gabriel.moise@gmail.com>
 * @package         Joomla
 * @subpackage      GiTags
 * @since           1.6
 * based on JTags   http://extensions.joomla.org/extensions/search-a-indexing/tags-a-clouds/5728 developed by Jacek Zielinski
 */

/* Check to ensure this file is included in Joomla! */
defined('_JEXEC') or die('Restricted access');

require_once( JPATH_COMPONENT.DS.'controller.php' );

JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS. 'com_gitags'.DS.'tables');

echo '<div class="componentheading">'.JText::_('All tags').'</div>';



$controller	= JController::getInstance('GiTags');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>