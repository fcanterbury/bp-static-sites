<?php
/**
 * @package	Joomla
 * @subpackage	GiTags
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

/* Check to ensure this file is included in Joomla! */
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );


class GiTagsModelAll extends JModel
{
	var $_tags = null;
	var $_ex = null;

	/**
	* Gets tags list from database
	*
	*/
	function getList()
	{
		if(!$this->_tags)
		{
			$user =& JFactory::getUser();
			$query = 'SELECT #__gitags_tags.tag_id, name, COUNT( #__gitags_tags.tag_id ) AS used, publish_down
				  FROM #__gitags_items
				  NATURAL JOIN #__gitags_tags
				  JOIN #__content ON (#__gitags_items.item_id = #__content.id)
				  WHERE state = 1 AND NOW() > publish_up
				  AND access <= '.(int) $user->get( 'aid' ).'
				  GROUP BY #__gitags_tags.tag_id
				  ORDER BY name ASC';
			$this->_tags = $this->_getList($query, 0, 0);
		}

		return $this->_tags;
	}

	/**
	* Gets minimum or maximum used tag
	*
	* @access	public
	* @param 	Int			$ext			Decides if get min or max
	* @return 	Int			$ex				Minimum or maximum used tag
	*/
	function getExtremum($ext)
	{
			$query = 'SELECT COUNT( tag_id ) AS used' .
					' FROM #__gitags_items' .
					' GROUP BY tag_id';
			if ($ext == 'max') $query .= ' ORDER BY used DESC';
			else $query .= ' ORDER BY used ASC';
			$this->_db->setQuery($query);
      		$this->_ex = $this->_db->loadResult();

		return $this->_ex;
	}
}

?>