<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

function GiTagsBuildRoute(&$query)
{
  $segments = array();

  if(isset($query['view']))
  {
    $segments[] = $query['view'];
    unset($query['view']);
  }

  if(isset($query['searchword']))
  {
    $segments[] = $query['searchword'];
    unset($query['searchword']);
  }

  if(isset($query['areas']))
  {
    @$segments[] = $query['areas[0]'];
    unset($query['areas']);
  }

  return $segments;
}

function GiTagsParseRoute($segments)
{
  $vars = array();

  if ($segments[0] == "all") $vars['view'] = "all";
  else
  {
	  $vars['option'] = "com_search";
	  $vars['searchword'] = $segments[0];
	  $vars['areas'][0] = "gitags";
  }

  return $vars;
}
?>
