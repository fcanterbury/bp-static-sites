<?php
/* Check to ensure this file is included in Joomla! */
defined( '_JEXEC' ) or die( 'Restricted access' );
$attribs['rel'] = "tag nofollow";
foreach($this->list as $l):
	if ($l->notToShow == 1) continue;
	$link = JRoute::_('index.php?option='.$this->com.'&searchword=' .
					$l->formattedName .'&areas[]=gitags');
	$text = JHTML::link($link, "<span style=\"font-size:".$l->fontSize."px;\" >".$l->name."</span>", $attribs)." (".$l->used.") ";
 	echo $text;
endforeach;
?>