<?php
/**
* @package	Joomla
* @subpackage	GiTags
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license	GNU/GPL, see LICENSE.php
*
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

/* Check to ensure this file is included in Joomla! */
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');

/**
 * @author	Gabi Moise <gabi.moise@gmail.com>
 * @package	Joomla
 * @subpackage	GiTags
*/
class GiTagsViewAll extends JView
{

	function display($tpl = null)
	{
		
                $mainframe = JFactory::getApplication();
		/* indispensable to determine if user is using sh404sef */
		@ include( 'administrator'.DS.'components'.DS.'com_sh404sef'.DS.'config'.DS.'config.sef.php' );
		
		$model = &$this->getModel();
		$list = $model->getList();

		$max = $model->getExtremum('max');
		$min = $model->getExtremum('min');
		$offset = $mainframe->getCfg('offset');
		
                $Enabled = 0;
		if ($Enabled==1) $com = "com_search";
		else
		{
			if ($mainframe->getCfg('sef') == 1)
				$com = "com_gitags";
			else $com = "com_search";
		}

		for($i = 0; $i < count($list); $i++)
		{
			$row =& $list[$i];
			$row->formattedName = str_replace(" ", "+", substr($row->name, 0, 20));
			$row->fontSize = $this->getFontSize($min, $max, $row->used, 11, 23);
			if ($row->publish_down != '0000-00-00 00:00:00' && (time() > strtotime($row->publish_down)+( $offset * 60 * 60))) $row->notToShow = 1;
			else $row->notToShow = 0;
		}

		$this->assignRef('list', $list);
		$this->assignRef('com', $com);

		parent::display($tpl);
	}

	/**
	* Calculates font size depending on how many times tag was used
	*
	* @access	public
	* @param 	Int			$min			The least used tag
	* @param 	Int			$max			The most used tag
	* @param 	Int			$used			How much times tag was used
	* @param 	Int			$minFontSize	minimal font size
	* @param 	Int			$maxFontSize	maximal font size
	* @return 	Int			$fontSize		font size
	*/
	function getFontSize($min, $max, $used, $minFontSize, $maxFontSize)
	{
		$difference = $max - $min;
		if ($used == $min) return $minFontSize;
		else if ($used == $max) return $maxFontSize;
		else
		{
			$x = ($maxFontSize - $minFontSize) / $difference;
			$used -= $min;
			return round($minFontSize + ($used * $x));
		}
	}
}

?>