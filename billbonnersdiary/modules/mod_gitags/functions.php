<?php
defined('_JEXEC') or die('Restricted access');

function getFontSize($min, $max, $used, $minFontSize, $maxFontSize)
{
	$difference = $max - $min;
	if ($used == $min) return $minFontSize;
	else if ($used == $max) return $maxFontSize;
	else
	{
		$x = ($maxFontSize - $minFontSize) / $difference;
		$used -= $min;
		return round($minFontSize + ($used * $x));
	}
}
?>