<?php

/**
 * GiTags Module
 *
 * @author          Gabriel Moise <gabriel.moise@gmail.com>
 * @package         Joomla
 * @subpackage      GiTags
 * @since           1.6
 * based on JTags   http://extensions.joomla.org/extensions/search-a-indexing/tags-a-clouds/5728 developed by Jacek Zielinski
 */

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.html.parameter' );
$mainframe = JFactory::getApplication();

require_once( 'modules'.DS.'mod_gitags'.DS.'functions.php' );

$minFontSize = $params->get('minFontSize', '11');
$maxFontSize = $params->get('maxFontSize', '23');
$limit = $params->get('limit', '25');$limit=25;
$catId = $params->get('category', '0');
$sectionId = $params->get('section', '0');

$db =& JFactory::getDBO();
$user =& JFactory::getUser();

/* retrieve max and min used tags */
$query = 'SELECT count( tag_id ) AS used
				FROM #__gitags_items
				JOIN #__content ON (#__gitags_items.item_id = #__content.id)
				WHERE state = 1 AND NOW() > publish_up ';
				//AND access <= '.(int) $user->get( 'aid' );
if ($sectionId != 0) $query .= ' AND #__content.sectionid = '.$sectionId;
if ($catId != 0) $query .= ' AND #__content.catid = '.$catId;
$query .= ' GROUP BY #__gitags_items.tag_id
				ORDER BY used DESC
				LIMIT '.$limit;
$db->setQuery($query);
$useArray = $db->loadResultArray();
$max = $useArray[0];
$min = $useArray[sizeof($useArray)-1];

$query = 'SELECT * FROM
		  (SELECT #__gitags_tags.tag_id, name, COUNT( #__gitags_tags.tag_id ) AS used, publish_down
				  FROM #__gitags_items
				  NATURAL JOIN #__gitags_tags
				  JOIN #__content ON (#__gitags_items.item_id = #__content.id)
				  WHERE state = 1 AND NOW() > publish_up ';
				  //AND access <= '.(int) $user->get( 'aid' );
//if ($sectionId != 0) $query .= ' AND #__content.sectionid = '.$sectionId;
if ($catId != 0) $query .= ' AND #__content.catid = '.$catId;
$query .= ' GROUP BY #__gitags_tags.tag_id
				  HAVING used >= '. $min .'
				  ORDER BY used DESC
				  LIMIT '.$limit.') AS DerivedTable
				  ORDER BY name ASC';

$db->setQuery($query);
$rows = $db->loadObjectList();
$offset = $mainframe->getCfg('offset');

/* indispensable to determine if user is using sh404sef */
@ include( 'administrator'.DS.'components'.DS.'com_sh404sef'.DS.'config'.DS.'config.sef.php' );
$Enabled  = 0;
if ($Enabled==1) $com = "com_search";
else
{
	if ($mainframe->getCfg('sef') == 1)
		$com = "com_gitags";
	else $com = "com_search";
}
		
$attribs['rel'] = "tag nofollow";

if (!$max) 
{
    echo JText::_('no tagged');
}
else
foreach($rows as $row)
{      
	if ($row->publish_down != '0000-00-00 00:00:00' && (time() > strtotime($row->publish_down)+( $offset * 60 * 60))) continue;
	$formattedName = str_replace(" ", "+", substr($row->name, 0, 21));
	$size = getFontSize($min, $max, $row->used, $minFontSize, $maxFontSize);
	echo JHTML::link(JRoute::_('index.php?option='.$com.'&searchword=' .$formattedName . '&areas[0]=gitags'),
  					'<span style="font-size:'.$size.'px;" >' . $row->name .'</span>', $attribs).' ';
}
//echo '<p align="right">'.JHTML::link(JRoute::_('index.php?option=com_gitags'), '+ '.JText::_('All tags'));
echo '</p>';
?>
