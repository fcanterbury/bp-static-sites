<?php
/**
 * @version   $Id: mod_roktabs.php 8643 2013-03-21 02:24:31Z steph $
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 * */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Include the syndicate functions only once
require_once (dirname(__FILE__) . '/helper.php');

JHtml::_('behavior.framework', true);

$conf = JFactory::getConfig();
if ($conf->get('caching') && $params->get('module_cache')) {
	$cache = JFactory::getCache('mod_roktabs');
	$list  = $cache->call(array('modRokTabsHelper', 'getList'), $params);
} else {
	$helper = new modRokTabsHelper();
	$list   = $helper->getList($params);
}

require(JModuleHelper::getLayoutPath('mod_roktabs'));