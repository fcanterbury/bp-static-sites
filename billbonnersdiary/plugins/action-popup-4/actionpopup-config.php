<?php

// Action PopUp 2.39 by Robert Plank
// www.ActionPopUp.com

// You don't need to edit this file if you're using it as a WordPress plugin,
// just activate the Action PopUp in your dashboard, then go to Settings,
// and click the Action PopUp link.

// If you are using Action PopUp on a regular site (like a static sales letter)
// you will have to edit this file.

// What color should the faded background be when a popup appears?
// Choices: maroon, navy, black, white, orange
$lightboxColor = "black";

// Width of your popup box
$width = "600";

// Height of your popup box
$height = "350";

// How far from the left of the screen should the popup appear?
$x = "50%";

// How far from the top of the screen should the popup appear?
$y = "50%";

// URL to send the user if they have filled out the opt-in form
// If you leave this blank, it will keep the user on the same page.
$thankYouURL = "";

// Alert box message to show after the user has been subscribed.
$thankYouMessage = "";

// If you want to receive 50% commissions on all Action PopUp sales,
// enter your Clickbank ID to provide an affiliate link to
// Action PopUp!
$clickbankID = "";

$subscribePrompt = "Would you like to subscribe to Robert Plank's newsletter to receive future updates?

Clicking 'OK' will do it!";

// Show the popup X number of times -- even if they don't subscribe?
// Set to "0" to keep showing the popup until they subscribe.
// It is HIGHLY recommended you keep this at 0.
$actionLimit = "0";

// How many long remember the action limit, i.e. 10 days
$actionTimeout = "6 hours";

// How many seconds until the popup gets activated
$actionDelay = 0;

// Show the "close" button on the popup?
$actionShowClose = true;

?>