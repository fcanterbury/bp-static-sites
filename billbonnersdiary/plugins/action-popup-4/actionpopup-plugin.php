<?php

/* 
 * Plugin Name:   Action PopUp
 * Version:       2.3.9
 * Plugin URI:    http://www.actionpopup.com
 * Description:   Show an unblockable exit pop-up on your blog.
 * Author:        Robert Plank
 * Author URI:    http://www.robertplank.com
 */

// Make it work with WordPress before version 2.6
if (!defined('WP_CONTENT_URL')) {
   define('WP_CONTENT_URL', get_option('siteurl') . '/wp-content');
}

//define('ACTIONPOPUP_URL', WP_CONTENT_URL . "/plugins/action-popup");
define('ACTIONPOPUP_URL',  eregi_replace("^" . $_SERVER["DOCUMENT_ROOT"], "", dirname(__FILE__)));

if (!function_exists('javascriptLine')) {
   function javascriptLine($input) {
      $input = preg_replace("/'/", "\'", $input); // Escape slashes
      $lines = preg_split("/(\r?\n)/si", $input);    // Separate into each line
      $lines = implode("\\n", $lines); // Turn back into a string

      return $lines;
   }
}

if (!class_exists("ActionTarget")) {
   class ActionTarget {
      // Load all of our keywords from the ad-templates folder.
      function findKeywords($dir=null) {
         // Get the list of all *.html files in that folder

         if (!is_dir($dir)) {
            $dir = dirname(__FILE__) . "/ad-templates";
         }
         $files = glob("$dir/*.html");

         $return = array();
         foreach ($files as $file) {
            $return[$file] = cleanFilename($file);
         }
         return $return;
      }

      // Turn a filename into a keyword, i.e. list-building.html
      // becomes: list building
      function cleanFilename($filename) {
         $filename = basename($filename);
         $filename = preg_replace('@\..*?$@', '', $filename);
         $filename = str_replace('-', ' ', $filename);
         return $filename;
      }

      // Strip all HTML tags from a document, including
      // JavaScript, CSS, and comments
      function html2txt($input){
         $javascriptRegex = '@<script[^>]*?>.*?</script>@si';
         $cssRegex = '@<style[^>]*?>.*?</style>@siU';
         $htmlRegex = '@<[\/\!]*?[^<>]*?>@si';
         $commentsRegex = '@<![\s\S]*?--[ \t\n\r]*>@';

         $search = array($javascriptRegex, $cssRegex, $htmlRegex, $commentsRegex);
         return preg_replace($search, '', $input);
      }

      function matchKeyword($keyword, &$contents) {
         if (is_array($keyword)) {
            foreach ($keyword as $item) {
               if (ActionTarget::matchKeyword($item, $contents)) { return true; }
            }
            return false;
         }

         if (empty($keyword)) { return false; }
         return preg_match('/\b' . preg_quote($keyword) . '\b/si', $contents);
      }

   }
}

class ActionPopUpWP {

   var $placed;
   var $postID;

   function __construct() {
      $this->ActionPopUpWP();
   }

   function ActionPopUpWP() {
      $this->placed = false;
      $this->postID = 0;
   }

   function setupMenu() {
      // Add link to options page
      add_options_page('Action PopUp Settings', 'Action PopUp', 10, __FILE__, array('ActionPopUpWP', 'menu'));

      if (function_exists("add_meta_box")) {
         // Add meta box
         add_meta_box('actionpopup-meta', 'Action PopUp', array(&$this, "meta"), "post");
         add_meta_box('actionpopup-meta', 'Action PopUp', array(&$this, "meta"), "page");
      }
   }

   function saveSettings($postID, $post=NULL) {
      global $wpdb;

      if ($post == NULL) { return; }
      if (function_exists("wp_is_post_autosave") && wp_is_post_autosave($postID)) { return; }

      if (function_exists("wp_is_post_revision") && ($postRevision = wp_is_post_revision($postID))) {
         $postID = $postRevision;
      }

      if (!function_exists("update_post_meta")) { return; }

      if (isset($_POST["action-disable"])) {
         if (!update_post_meta($postID, "action-disable", "1")) {
            add_post_meta($postID, "action-disable", "1", true);
         }
      }
      else {
         delete_post_meta($postID, "action-disable");
      }
   	//extract($_POST);
		//$wpdb->query("UPDATE $wpdb->posts SET robotsmeta = '$robotsmeta' WHERE ID = $pID");
   }

   function meta() {
      global $post;

      echo '<label>';
      if (get_post_meta($post->ID, 'action-disable', true)) {
         echo '<input type="checkbox" name="action-disable" checked="checked"> Disable the popup on this page?';
      }
      else {
         echo '<input type="checkbox" name="action-disable"> Disable the popup on this page?';
      }
      echo '</label>';
   }

   function start() {
      ob_start(array(&$this, 'insert'));
   }

   function scripts() {
      //wp_enqueue_script('colorpicker');
      wp_enqueue_script('jquery');
      wp_enqueue_script('jquery-ui-core');
   }

   function clear() {
      if (isset($_POST["actionpopup_limit"])) {
         setcookie("actionCount", "", time()-3600, "/");
         setcookie("firstname", "", time()-3600, "/");
      }
   }

   function insert($buffer) {
      if ($this->placed) { return $buffer; }
      //$this->placed = true;

      // Decide if we need to target based on keywords for this page.
      if ($actionpopup_target = get_option("actionpopup_target")) { }
      else { $actionpopup_target = "none"; }
      //return "target = $actionpopup_target";

      $keywords = array_map("trim", explode("\n", get_option("actionpopup_keywords")));
      $this->body($buffer);

      // If no targeting, show the popup
      if ($actionpopup_target == "none") {
         $code .= $this->code();
      }
      elseif ($actionpopup_target == "include" || $actionpopup_target == "exclude") {
         if ($actionpopup_target == "include" && ActionTarget::matchKeyword($keywords, $this->text)) {
            $code .= $this->code();
         }
         elseif ($actionpopup_target == "exclude" && !ActionTarget::matchKeyword($keywords, $this->text)) {
            $code .= $this->code();
         }
      }

      $match = '@(</html>)@si';      
      if (preg_match($match, $buffer)) {
         return preg_replace($match, $code . '\\1', $buffer);
      }
      else {
         return $buffer . $code;
      }
      //return $buffer;
   }

   function body($buffer) {
      global $actionID, $post;
      if ((is_single() || is_page()) && $actionID == 0) { $actionID = $post->ID; }

      // Get body text
      $text = ActionTarget::html2txt($buffer);

      // Add the URL as a keyword
      $parse = parse_url($_SERVER["REQUEST_URI"]);
      $uri = $parse["path"];
      $uri = preg_replace('@(^/|/$)@', '', $uri);

      $this->text = $uri . "\n" . $text;
      return $buffer;
   }

   function footer() {
      global $actionID;

      echo $this->code();
   }

   function code() {
      global $actionID;

      if ($actionID > 0 && get_post_meta($actionID, 'action-disable', true)) { return ""; }

      if ($this->placed) { return ""; }
      else { $this->placed = true; }

      $path = ACTIONPOPUP_URL;

      $code = "";

      //$code .= '<script type="text/javascript">' . "\n";
      //$code .= "\n\n";

      //$code .= "var jQuery;\n";
      //$code .= "if (jQuery != undefined) { jQuery.noConflict(); }\n";

      //$code .= "// -->\n";
      //$code .= "</script>\n\n";

      if ($url = get_option("actionpopup_url")) {
         $body = '<iframe src="' . $url . '" width="100%" height="100%" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>';
      }
      else {
         $body = get_option("actionpopup_code");
         if (!$body) { $body = "Enter your popup code here..."; }
      }

      $code .= '<div id="actionTemplate" style="display:none;">' . "\n";
      $code .= $body;
      $code .= "\n" . '</div>' . "\n\n";

      $code .=  '<script type="text/javascript">' . "\n";
      $code .= '/* <![CDATA[ *' . '/' . "\n";

      // Show color settings...
      $code .= "var actionShowClose = " . (get_option("actionpopup_close") === "" ? "false" : "true") . ";\n";
      if (get_option("actionpopup_color")) { $code .= "var actionColor = '" . get_option("actionpopup_color") . "';\n"; }

      // Size
      if (get_option("actionpopup_width") !== FALSE) { $code .= "var actionWidth = '" . get_option("actionpopup_width") . "';\n"; }
      if (get_option("actionpopup_height") !== FALSE) { $code .= "var actionHeight = '" . get_option("actionpopup_height") . "';\n"; }

      // Position
      if (get_option("actionpopup_x") !== FALSE) { $code .= "var actionLeft = '" . get_option("actionpopup_x") . "';\n"; }
      if (get_option("actionpopup_y") !== FALSE) { $code .= "var actionTop = '" . get_option("actionpopup_y") . "';\n"; }

      if (get_option("actionpopup_effect") !== FALSE) { $code .= "var actionEffect = '" . get_option("actionpopup_effect") . "';\n"; }

      if (get_option("actionpopup_limit") !== FALSE) { $code .= "var actionLimit = '" . get_option("actionpopup_limit") . "';\n"; }
      if (get_option("actionpopup_timeout") !== FALSE) { $code .= "var actionTimeout = '" . get_option("actionpopup_timeout") . "';\n"; }

      if (get_option("actionpopup_delay") !== FALSE) { $code .= "var actionDelay = '" . get_option("actionpopup_delay") . "';\n"; }

      //if (get_option("actionpopup_code")) { $code .= "var actionTemplate = '" . javascriptLine(get_option("actionpopup_code")) . "';\n"; }
      if (get_option("actionpopup_affiliate") != "") { $code .= "var actionAffiliate = '" . (get_option("actionpopup_affiliate")) . "';\n"; }

      if (get_option("actionpopup_thanks") != "") { $code .= "var actionThanks = '" . javascriptLine(addslashes(get_option(strip_tags("actionpopup_thanks")))) . "';\n"; }
      if (get_option("actionpopup_redirect") != "") { $code .= "var actionRedirect = '" . addslashes(javascriptLine(get_option(strip_tags("actionpopup_redirect")))) . "';\n"; }

      $code .= '/' . '* ]]> *' . '/' . "\n";
      $code .= '</script>' . "\n\n";

      $code .= '<script type="text/javascript" src="' . $path . '/actionpopup.php?wordpress=1"></script>' . "\n";
      
      // Show onload code if set
      if (($actionpopup_trigger = get_option("actionpopup_trigger")) || (get_option("actionpopup_capture") == "1")) {
         $code .=  '<script type="text/javascript">' . "\n";
         //$code .= '/* <![CDATA[ *' . '/' . "\n";
         if ($actionpopup_trigger == "onload") { $code .= 'ActionPopup.onEnter = true;' . "\n"; }
         if (get_option("actionpopup_capture") == "1") { $code .= "Link.capture();\n"; }
         //$code .= '/' . '* ]]> *' . '/' . "\n";
         $code .= '</script>' . "\n\n";
      }

      if ($css = get_option("actionpopup_css")) {
         $css = strip_tags($css);
         if (!eregi(";", $css)) { $css .= ";"; }

         $css = addslashes(preg_replace("@\s+@si", " ", $css));
         $css = preg_replace("@!(.*?);@si", ";", $css);
         $css = preg_replace("@;@si", " !important;", $css);

         $code .= '<style type="text/css">' . "\n";
         $code .= "<!--\n";
         $code .= '#actionPopup {' . "\n";
         $code .= $css . "\n";
         $code .= '}' . "\n";
         $code .= '/'  . '/' . "-->\n";
         $code .= "</style>\n";

         $code .=  '<script type="text/javascript">' . "\n";
         $code .= '/* <![CDATA[ *' . '/' . "\n";
         //$code .= 'jQuery.cssRule(".actionPopup", "' . $css . '");' . "\n";
         $code .= '/' . '* ]]> *' . '/' . "\n";
         $code .= '</script>' . "\n\n";
      }

      return $code;
   }

   function menu() {
      ?>
      <div class="wrap">
      <h2>Action PopUp Settings</h2>

      <script type="text/javascript" src="<?php echo ACTIONPOPUP_URL; ?>/farbtastic.js"></script>
      <link rel="stylesheet" href="<?php echo ACTIONPOPUP_URL; ?>/farbtastic.css" type="text/css" />

      <form method="post" action="options.php">

      <?php wp_nonce_field('update-options'); ?>
      <input type="hidden" name="action" value="update" />
      <input type="hidden" name="page_options" value="actionpopup_code,actionpopup_css,actionpopup_url,actionpopup_color,actionpopup_trigger,actionpopup_capture,actionpopup_width,actionpopup_height,actionpopup_x,actionpopup_y,actionpopup_limit,actionpopup_timeout,actionpopup_delay,actionpopup_affiliate,actionpopup_thanks,actionpopup_redirect,actionpopup_close" />

      <p>This will show an unblockable exit popup on your blog...</p>

      <table border="0" width="100%">

      <tr>
         <td width="65%" valign="top">
            <h3>HTML Code</h3>

            <p>Paste the cut-n-paste code your autoresponder gave you... this will appear on your popup.</p>

         </td>

         <td valign="top">
            <h3>CSS Code (Optional)</h3>

            <p>Do not include <code>&lt;STYLE&gt;</code> tags!<br />
            Example: <code>border:none;</code>
            </p>
         </td>
      </tr>

      <tr>
         <td valign="top">
            <textarea name="actionpopup_code" class="code" cols="60" rows="10" style="width: 98%; font-size: 12px;"><?php echo get_option("actionpopup_code"); ?></textarea>
         </td>

         <td valign="top">

            <textarea name="actionpopup_css" class="code" cols="30" rows="10" style="width: 98%; font-size: 12px;"><?php echo get_option("actionpopup_css"); ?></textarea>
   
         </td>
         </tr>

      <tr>
         <td colspan="2">
            <h3>Optional: Specify an External URL</h3>

            <p>If your signup code conflicts with the popup, enter the URL of an external page and it will show in an iframe instead of on the actual page.  (Leave blank to use the HTML code instead.)<br />
            Example: <code>http://www.google.com</code>
            </p>

            <p>
            <textarea name="actionpopup_url" class="code" cols="60" rows="2"><?php echo get_option("actionpopup_url"); ?></textarea>
            </p>


         </td>
      </tr>
      </table>

      <p><input type="submit" class="button" value="Save All Changes" /></p>

      <h3>PopUp Settings</h3>

      <p>Customize colors, position, effects, recurrence, and more.</p>

      <table class="form-table">

         <tr valign="top">
            <th scope="row">Lightbox Color</th>
            <td>
               <select name="actionpopup_setcolor" id="actionpopup_setcolor" onchange="jQuery('#actionpopup_color').val(this.value).focus();">
               <?php

               if ($actionpopup_color = get_option("actionpopup_color")) { }
               else { $actionpopup_color = "#000080"; }
            
               $defaultColors = array(
                  "" => "Choose My Own Color",
                  "#800000" => "Maroon",
                  "#000080" => "Navy",
                  "#000000" => "Black",
                  "#FFFFFF" => "White",
                  "#FFA500" => "Orange",
                  "transparent" => "Transparent"
               );

               ?>

               <?php foreach ($defaultColors as $colorValue => $colorName): ?>
                  <?php if ($actionpopup_color == $colorValue) : ?>
                     <option value="<?php echo $colorValue ?>" selected="selected"><?php echo $colorName ?></option>
                  <?php else: ?>
                     <option value="<?php echo $colorValue ?>"><?php echo $colorName ?></option>
                  <?php endif; ?>
               <?php endforeach; ?>

               </select>

               <small>(hex code - # is required)</small> <input type="text" name="actionpopup_color" id="actionpopup_color" size="9" value="<?php echo $actionpopup_color; ?>" /><br />
               What &quot;faded out&quot; color should show behind the popup?
            </td>

            <td rowspan="2" align="center"><div id="colorpicker"></div></td>

         </tr>

         <tr>
            <th scope="row">Trigger</th>
            <td>
               <?php

               if ($actionpopup_trigger = get_option("actionpopup_trigger")) { }
               else { $actionpopup_trigger = "onexit"; }
            
               $defaultTriggers = array(
                  "onexit" => "Visitor is about to <b>click away</b> from the page.",
                  "onload" => "Visitor <b>first loads</b> the page.",
               );

               ?>

               <?php foreach ($defaultTriggers as $triggerValue => $triggerName): ?>
                  <label>
                  <?php if ($actionpopup_trigger == $triggerValue) : ?>
                        <input type="radio" name="actionpopup_trigger" value="<?php echo $triggerValue ?>" checked="checked" /><?php echo $triggerName ?>
                  <?php else: ?>
                     <input type="radio" name="actionpopup_trigger" value="<?php echo $triggerValue ?>" /><?php echo $triggerName ?>
                  <?php endif; ?>
                  </label><br />
               <?php endforeach; ?>

               <?php
                  $actionCaptureCheck = (get_option("actionpopup_capture") == 1) ? 'checked="checked"' : '';
                  $actionCaptureAlert = javascriptLine(
                     "Want to EXCLUDE specific links from the exit pop-up (like order links?)\n" . 
                     "<a href='http://www.example.com' onclick='exit=false'>Order Here</a>\n\n" .

                     "Want to only INCLUDE some of your links?\n" . 
                     "<a href='http://www.example.com' onclick='ActionPopup.show()'>A link to some other page</a>"

                  );
               ?>

               <label><input type="checkbox" name="actionpopup_capture" value="1" <?php echo $actionCaptureCheck ?> /> Visitor clicks an offsite link.</label>  <a href="#" onclick="alert('<?php echo $actionCaptureAlert; ?>'); return false;">(Help)</a>
            </td>
         </tr>

         <tr>
            <th scope="row">Size &amp; Position</th>

            <?php

            if ($actionpopup_width = get_option("actionpopup_width")) { }
            else { $actionpopup_width = "200"; }

            if ($actionpopup_height = get_option("actionpopup_height")) { }
            else { $actionpopup_height = "200"; }

            ?>

            <td>
               <label>Width: <input type="text" name="actionpopup_width" id="actionpopup_width" value="<?php echo $actionpopup_width; ?>" size="3" /></label> <label>Height: <input type="text" name="actionpopup_height" id="actionpopup_height" value="<?php echo $actionpopup_height; ?>" size="3" /></label><br />

               <a href="#" onclick="actionPreset('actionpopup_width', 'actionpopup_height', 150, 150); return false;">Small</a>,
               <a href="#" onclick="actionPreset('actionpopup_width', 'actionpopup_height', 200, 200); return false;">Medium</a>,
               <a href="#" onclick="actionPreset('actionpopup_width', 'actionpopup_height', 375, 400); return false;">Large</a>...
            </td>

            <?php

            if (($actionpopup_x = get_option("actionpopup_x")) !== FALSE) { }
            else { $actionpopup_x = "50"; }

            if (($actionpopup_y = get_option("actionpopup_y")) !== FALSE) { }
            else { $actionpopup_y = "200"; }

            ?>

            <td>
               <label>X: <input type="text" name="actionpopup_x" id="actionpopup_x" value="<?php echo $actionpopup_x; ?>" size="9" /></label> <label>Y: <input type="text" name="actionpopup_y" id="actionpopup_y" value="<?php echo $actionpopup_y; ?>" size="9" /></label><br />

               <a href="#" onclick="actionPreset('actionpopup_x', 'actionpopup_y', 50, 50); return false;">Top Left</a>,
               <a href="#" onclick="actionPreset('actionpopup_x', 'actionpopup_y', '100%', '100%'); return false;">Top Right</a>,
               <a href="#" onclick="actionPreset('actionpopup_x', 'actionpopup_y', '50%', '50%'); return false;">Center</a>...<br />

               <a href="#" onclick="actionPreset('actionpopup_x', 'actionpopup_y', 0, 0); actionPreset('actionpopup_width', 'actionpopup_height', '100%', 150); return false; ">Dock on Top</a>,
               <a href="#" onclick="actionPreset('actionpopup_x', 'actionpopup_y', 0, '100%'); actionPreset('actionpopup_width', 'actionpopup_height', '100%', 150); return false; ">Dock on Bottom</a>,
               <a href="#" onclick="actionPreset('actionpopup_x', 'actionpopup_y', '10', 300); actionPreset('actionpopup_width', 'actionpopup_height', '200', '175'); return false; ">Dock Left</a>
            </td>

         </tr>

         <tr>
            <th scope="row">Recurrence &amp; Delay</th>
            <td>

            <select name="actionpopup_limit" id="actionpopup_limit" onchange="document.getElementById('actionpopup_timeout').style.display = (this.value == '0') ? 'none' : 'inline';">
               <?php

               $actionpopup_limit = get_option("actionpopup_limit");
            
               $defaultLimits = array(
                  "0" => "Always Show",
                  "1" => "Show Once",
                  "2" => "Show Twice",
                  "3" => "Show 3 Times",
                  "4" => "Show 4 Times",
                  "5" => "Show 5 Times",
                  "6" => "Show 6 Times",
                  "7" => "Show 7 Times",
                  "8" => "Show 8 Times",
                  "9" => "Show 9 Times",
                  "10" => "Show 10 Times"
               );

               ?>

               <?php foreach ($defaultLimits as $limitValue => $limitName): ?>
                  <?php if ($actionpopup_limit == $limitValue) : ?>
                     <option value="<?php echo $limitValue ?>" selected="selected"><?php echo $limitName ?></option>
                  <?php else: ?>
                     <option value="<?php echo $limitValue ?>"><?php echo $limitName ?></option>
                  <?php endif; ?>
               <?php endforeach; ?>

            </select>

            <select name="actionpopup_timeout" id="actionpopup_timeout">
               <?php

               $actionpopup_timeout = get_option("actionpopup_timeout");
            
               $defaultTimeouts = array(
                  "10 minutes" => "Every 10 Minutes",
                  "30 minutes" => "Every 30 Minutes",
                  "1 hour" => "Every Hour",
                  "3 hours" => "Every 3 Hours",
                  "8 hours" => "Every 8 Hours",
                  "12 hours" => "Every 12 Hours",
                  "1 day" => "Every Day",
                  "2 days" => "Every 2 Days",
                  "3 days" => "Every 3 Days",
                  "5 days" => "Every 5 Days",
                  "7 days" => "Every Week",
                  "30 days" => "Every 30 Days",
                  "6 months" => "Every 6 Months"
               );

               ?>

               <?php foreach ($defaultTimeouts as $timeoutValue => $timeoutName): ?>
                  <?php if ($actionpopup_timeout == $timeoutValue) : ?>
                     <option value="<?php echo $timeoutValue ?>" selected="selected"><?php echo $timeoutName ?></option>
                  <?php else: ?>
                     <option value="<?php echo $timeoutValue ?>"><?php echo $timeoutName ?></option>
                  <?php endif; ?>
               <?php endforeach; ?>

            </select><br />

            <?php if (get_option("actionpopup_close") === ""): ?>
            <label><input type="checkbox" name="actionpopup_close" /> Show Close Button?</label>
            <?php else: ?>
            <label><input type="checkbox" name="actionpopup_close" checked="checked" /> Show Close Button?</label>
            <?php endif; ?>
               
            <br />
            
            How many times should the popup appear before they subscribe?
            </td>

            <td>

            <select name="actionpopup_delay" id="actionpopup_delay">
               <?php

               $actionpopup_delay = intval(get_option("actionpopup_delay"));
               
               $defaultDelays = array(
                  "0" => "Show Instantly",
                  "5" => "5 Second Delay",
                  "10" => "10 Second Delay",
                  "30" => "30 Second Delay",
                  "60" => "1 Minute Delay"
               );

               ?>

               <?php foreach ($defaultDelays as $delayValue => $delayName): ?>
                  <?php if ($actionpopup_delay == $delayValue) : ?>
                     <option value="<?php echo $delayValue ?>" selected="selected"><?php echo $delayName ?></option>
                  <?php else: ?>
                     <option value="<?php echo $delayValue ?>"><?php echo $delayName ?></option>
                  <?php endif; ?>
               <?php endforeach; ?>

            </select>
               
            <br />
            
            How long after loading the page should the popup activate?</td>
         </tr>

         <tr>

            <th scope="row">Thank You Message &amp; URL</th>

            <td>

               <b>Thank You Message</b><br />
               
               <textarea name="actionpopup_thanks" class="code" cols="50" rows="2" style="font-size: 12px;"><?php echo strip_tags(get_option("actionpopup_thanks")); ?></textarea>
               <br />
               What message to show when people subscribe?  Leave blank to disable.  (No HTML.)
            </td>

            <td>

               <b>Thank You URL</b><br />
               
               <textarea name="actionpopup_redirect" class="code" cols="50" rows="2" style="font-size: 12px;"><?php echo strip_tags(get_option("actionpopup_redirect")); ?></textarea>
               <br />
               What URL to redirect people to after signup?<br /> Leave blank to keep them on the same page.
            </td>
         </tr>

         <tr>
            <th scope="row">Clickbank ID</th>

            <td colspan="2">
               <input type="text" name="actionpopup_affiliate" size="15" value="<?php echo get_option("actionpopup_affiliate"); ?>" /><br />
               Your <a target="_blank" href="http://simplephp.reseller.hop.clickbank.net">Clickbank affiliate ID</a> if you want to receive commissions for Action PopUp.
            </td>
         </tr>


      </table>

      <p><input type="submit" class="button" value="Save All Changes" /></p>

      </form>

      <script type="text/javascript">
      /* <![CDATA[ */
         //document.getElementById('actionpopup_keywordBox').style.display = (document.getElementById('actionpopup_target').value == 'none') ? 'none' : 'inline';
         document.getElementById('actionpopup_timeout').style.display = (document.getElementById('actionpopup_limit').value == '0') ? 'none' : 'inline';

         function actionPreset(widthLayer, heightLayer, width, height) {
            document.getElementById(widthLayer).value = width;
            document.getElementById(heightLayer).value = height;
         }

         jQuery(function() {
            jQuery('#colorpicker').farbtastic('#actionpopup_color');
         });

         function action_halfWidth() {
            var width = parseInt(document.getElementById('actionpopup_width').value);
            width = Math.round(width/2);
            width = width + 40;

            return width;
         }

         function action_halfHeight() {
         }
      /* ]]> */
      </script>

      </div>

      <?php
   }

   function links($links, $file) {
      if ($file == plugin_basename(__FILE__)) {
         array_unshift($links, "<a href=\"options-general.php?page=$file\">Settings</a>");
      }
      return $links;
   }

}

if (function_exists('add_action')) {
   $actionID = 0;

   $apwp = new ActionPopUpWP();

   add_action('init', array($apwp, 'clear'));

   if (is_admin()) {
      add_action('admin_menu', array($apwp, 'setupMenu'));
      add_filter("plugin_action_links", array($apwp, 'links'), 10, 2);

      // Save settings on meta box
      add_filter("wp_insert_post", array($apwp, 'saveSettings'), 10, 2);

   }
   else {
      add_action('wp_print_scripts', array($apwp, 'scripts'));

      // Capture keywords
      add_filter('the_content', array($apwp, 'body'));

      $source = file_get_contents(get_theme_root() . '/' . get_template() . '/footer.php');

      if( strpos($source, 'wp_footer') === false ) {
         //No footer action in theme, tack onto WordPress footer
         add_action('wp_head', array($apwp, 'start'), 1000);
      }

      // Add to WordPress footer
      add_action('wp_footer', array($apwp, 'footer'), 5000);
   }
}

?>