<?php

// Action PopUp 2.39 by Robert Plank
// www.ActionPopUp.com

// Do not modify - edit actionpopup-config.php instead
// Installing onto WordPress?  Just upload this folder into your plugins folder
// and activate and configure in your Dashboard.
$configFile = dirname(__FILE__) . "/actionpopup-config.php";
$pop = $_GET['pop']; 

if (file_exists($configFile)) { require($configFile); }
require_once(dirname(__FILE__) . "/actionpopup-packer.php");

function getURL() {
   return "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]);
}

function javascriptCompress($buffer) {
   $myPacker = new JavaScriptPacker($buffer, 'Normal', true, false);
   return $myPacker->pack();
}

function javascriptLine($input) {
   $input = addslashes($input);

   $lines = preg_split("/[\r\n]+/si", $input);    // Separate into each line
   $lines = implode("", $lines); // Turn back into a string

   return $lines;
}

function javaScriptSafe($input) {
   $input = preg_replace("/\r?\n/si", "\\n", $input);
   return $input;
}

?>
<?php if (isset($_GET["css"])): ?>
<?php if (!is_int($_GET["css"])) { $lightboxColor = $_GET["css"]; } ?>
<?php header("Content-type:text/css"); ?>
#actionBlanket {
   position:absolute;
   position:fixed;
   background-color:<?php echo $lightboxColor ?>;
   width:100%; height:100%;
   top:0; left:0;
   z-index:10000 !important;
   display:none;
}

#actionPopup {
   border:solid 3px;
   position:absolute;
   background-color:white;
   padding:10px;
   z-index:11000 !important;
   display:none;
   vertical-align:middle;
   text-align:center;
}



<?php else: ?>
<?php

ob_start("javascriptCompress");

?>
<?php header("Content-type:text/javascript"); ?>
<?php
$host = "";
if (isset($_SERVER["HTTP_HOST"])) {
   $host = $_SERVER["HTTP_HOST"];
}

$refer = "";
if (isset($_SERVER["HTTP_REFERER"])) {
   $refer = $_SERVER["HTTP_REFERER"];
}

// Make sure we have all the files we need...
if (!file_exists(dirname(__FILE__) . "/actionpopup-include.php")) {
   echo 'alert("Please upload the ENTIRE action-popup folder before continuing...");';
   die();
}

ob_start();
if (function_exists("actionDisplay")) {
   actionDisplay();
}
else {
   //include("actionpopup-template.html");
   include('templates/' . $pop);
}
$popCode = ob_get_contents();
ob_end_clean();
$popCode = javascriptLine($popCode);

if (eregi('%$', $width)) { $showWidth = $width; }
else { $showWidth = intval($width) . "px"; }

if ($y < 0) { $topCode = 'bottom:' . $y . 'px'; }
else { $topCode = 'top:' . $y . 'px'; }

?>var Move = {

   browserHeight : function() {
      if (typeof window.innerWidth == 'number') {
         return window.innerHeight - 2;
      }
      else {
         if (document.documentElement && document.documentElement.clientHeight) {
            return document.documentElement.clientHeight;
         }
         else {
            if (document.body && document.body.clientHeight) {
               return document.body.clientHeight;
            }
         }
      }
   },

   browserWidth : function() {
      if (typeof window.innerWidth == 'number') {
         return window.innerWidth - 2;
      }
      else {
         if (document.documentElement && document.documentElement.clientWidth) {
            return document.documentElement.clientWidth;
         }
         else {
            if (document.body && document.body.clientWidth) {
               return document.body.clientWidth;
            }
         }
      }
   }

};

var actionTemplate;
var actionWidth, actionHeight, actionLeft, actionTop, actionColor, actionEffect, actionTimeout, actionLimit, actionDelay;
var actionThanks, actionRedirect;
var actionAffiliate;
var actionShowClose;

<?php require(dirname(__FILE__) . "/actionpopup-include.php"); ?>

try {
   if (jQuery("#actionTemplate").html() != null) {
      actionTemplate = jQuery("#actionTemplate").html();
   }

   if (actionWidth === undefined) { actionWidth = jQuery("#actionTemplate").width().toString(); }
   if (actionHeight === undefined) { actionHeight = jQuery("#actionTemplate").height().toString(); }

   if (actionLeft === undefined) { actionLeft = jQuery("#actionTemplate").css("left"); }
   if (actionTop === undefined) { actionTop = jQuery("#actionTemplate").css("top"); }
}
catch(_) { }

if (actionShowClose == undefined) { actionShowClose = <?php echo ($actionShowClose === false) ? "false" : "true"; ?>; }
if (actionRedirect == undefined) { actionRedirect = '<?php echo $thankYouURL ?>'; }
if (actionDelay === undefined) { actionDelay = '<?php echo intval($actionDelay); ?>'; }

if (actionTemplate === undefined) { actionTemplate = '<?php echo $popCode; ?>'; }
if (actionAffiliate === undefined) { actionAffiliate = '<?php echo $clickbankID; ?>'; }

if (actionTimeout === undefined) { actionTimeout = '<?php echo $actionTimeout; ?>'; }
if (actionLimit === undefined) { actionLimit = '<?php echo intval($actionLimit); ?>'; }

if (actionColor === undefined) { actionColor = '<?php echo $lightboxColor; ?>'; }
if (actionWidth === undefined) { actionWidth = '<?php echo $showWidth; ?>'; }
if (actionHeight === undefined) { actionHeight = '<?php echo $height; ?>'; }
if (actionLeft === undefined) { actionLeft = '<?php echo $x; ?>'; }
if (actionTop === undefined) { actionTop = '<?php echo $y; ?>'; }

if (actionThanks == undefined) { actionThanks = '<?php echo strip_tags($thankYouMessage); ?>'; }

var topCode;
var widthCode;

var topValue = 0;
var leftValue = 0;

var theHeight = 0;
var theWidth = 0;

if (actionWidth.match('%$')) { widthCode = jQuery(window).width() * (parseInt(actionWidth)/100); }
else { widthCode = parseInt(actionWidth, 10); }

widthCode = Math.min(widthCode, jQuery(window).width() - 25);
widthCode += 'px';

var actionPart;

// Calculate Y value
if (actionPart = actionTop.match('([0-9]+)%')) {
   var virtualTop = parseInt(actionPart, 10) / 100;
   theHeight = parseInt(actionHeight, 10);
   var halfTop = parseInt(theHeight/2, 10);
   var halfHeight = parseInt(jQuery(window).height() * virtualTop, 10);

   topValue += parseInt(halfHeight - halfTop, 10);
}
if (actionPart = actionTop.match('([-+]?[0-9]+)$')) {
   theHeight = actionPart[1];
   topValue += parseInt(theHeight, 10);
}

topValue = Math.max(0, topValue);
topValue = Math.min(topValue, jQuery(window).height() - theHeight - 40);
topCode = 'top:' + topValue + 'px';

// Calculate X value
if (actionPart = actionLeft.match('([0-9]+)%')) {
   theWidth = parseInt(actionWidth, 10);
   var virtualLeft = parseInt(actionPart, 10) / 100;
   var halfLeft = parseInt(theWidth/2, 10);
   var halfWidth = parseInt(jQuery(window).width() * virtualLeft, 10);

   leftValue += parseInt(halfWidth - halfLeft, 10);
}
if (actionPart = actionLeft.match('([-+]?[0-9]+)$')) {
   theWidth = actionPart[1];
   leftValue += parseInt(theWidth, 10);
}
leftValue = Math.max(0, leftValue);
leftValue = Math.min(leftValue, jQuery(window).width() - theWidth - 40);
leftCode = 'top:' + leftValue + 'px';

/*
if (actionLeft.match('%$')) {
   var virtualLeft = parseInt(actionLeft, 10) / 100;
   var theWidth = parseInt(actionWidth, 10);
   var halfLeft = parseInt(theWidth/2, 10);
   var halfWidth = parseInt(Move.browserWidth() * virtualLeft, 10);

   leftValue = parseInt(halfWidth - halfLeft, 10);
   leftCode = 'left:' + leftValue + 'px';
}
else if (actionLeft.match('^-')) {
   leftValue = parseInt(jQuery(document).width() - parseInt(actionWidth) + parseInt(actionLeft));
   leftCode = 'left:' + leftValue + 'px';
}
else {
   actionLeft = parseInt(actionLeft, 10)+10;
   leftValue = actionLeft;
   leftCode = 'left:' + leftValue + 'px';
}
*/

var popCode;

if (jQuery.browser.msie) { // jQuery.browser.msie
   popCode = '<div id="actionPopup" class="actionPopup" style="width:' + widthCode + '; height:' + actionHeight + 'px; display:none;">';
}
else {
   popCode = '<div id="actionPopup" class="actionPopup" style="width:' + widthCode + '; height:' + actionHeight + 'px; ' + leftCode + '; ' + topCode + '; display:none;">';
}
popCode += actionTemplate;

if (actionShowClose) {
   popCode += '<div style="margin-top: -385px; margin-right: -20px; color:#FF0000;" id="actionClose" class="accion"><p align="right">';
   popCode += '<a href="#" onclick="ActionPopup.hide(); return false;"><img src="http://www.taipanpublishinggroup.com/images/web/buttons/close.png"></a>';
}

if (actionAffiliate !== '') {
   popCode += '<br /><small><a href="http://' + actionAffiliate + '.actionpopup.com">Powered by Action PopUp</a></small>';
}

popCode += '</p></div>';

popCode += '</div>';

var myDomain = document.domain + "";
var yourDomain = "<?php echo $host ?>";

var domainReplace = new RegExp("^www\.", "gi");

String.prototype.capitalize = function() {
    return this.replace(/\w+/g, function(a){
        return a.charAt(0).toUpperCase() + a.substr(1).toLowerCase();
    });
};

var exit;

var scroll = 0;
var actionTimer = null;

// Link clicking functions
var Link = {

   to : null,

   disable : function() {
      Link.click = function() { return true; };
   },

   // Capture outgoing links
   capture : function() {
      for (i=0;i<document.links.length;i++) {
         var old = document.links[i].onclick;

         // An onclick event is already set for this link.
         if (old) { continue; }

         // The "exit" variable is set to false, so don't show the popup.
         if (exit === false) { continue; }

         // This is an anchor link, do not show the popup.
         if (document.links[i].href.match(/#/)) { continue; }

         if (!Link.offsite(document.links[i].href)) { continue; }

         // The popup has already been shown this session, do not show it again.
         if (ActionPopup.shown) { continue; }
            
         document.links[i].onclick = Link.click;
      }
   },

   click : function(sender) {
      if (sender && sender.href) {
         Link.to = sender.href;
      }
      else {
         Link.to = this.href;
      }
      ActionPopup.show();
      return false;
   },

   offsite : function(url) {

      var currentURL = document.location.href;

      // The domain of your web site (no http or www)
      var domain = document.location.host.replace(/^www\./gi, '');

      // If there is a slash at the beginning of the link, it's a local link
      if (url.match(/^\//)) { return false; }

      // If the URL is on our domain, it's not an offsite link
      if (url.match(domain)) { return false; }

      // If the URL contains a colon, i.e. "http://someothersite.com", it's offsite
      if (url.match(/^[a-z]+:/gi)) { return true; }

      // Otherwise, it's a local link (i.e. "page.html")
      return false;
   }

};

// Pop-up specific functions
var ActionPopup = {

   active : false,
   shown : false,
   onEnter : false,
   scroll : 0,
   shownThanks : false,

   thanks : function() {
      // Make sure we only show this once
      if (shownThanks === true) { return false; }
      if (actionThanks == undefined || actionThanks == '') { return false; }
      alert(actionThanks);
      shownThanks = true;
   },

   brand : function(layerClass, contents) {
      // Brand the first name on the sales letter
      jQuery("." + layerClass).each(
         function() {
            if (jQuery(this).html() != "") {
               jQuery(this).html(contents);
            }
         }
      );
   },

   init : function() {
      // Show the opt-in pop-up that asks for their first name
      jQuery().mousemove(handleMove);
      if (ActionPopup.onEnter) { ActionPopup.show(); }
   },

   // Immediately show the popup
   show : function(delay) {
      if (ActionPopup.shown) { return false; }
      if (delay !== undefined) {
         setTimeout(function() { ActionPopup.show(); }, delay*1000);
         return;
      }

      ActionPopup.shown = true;

      var actionCount = parseInt(ActionPopUpCookie.get("actionCount"), 10);

      if (isNaN(actionCount)) { actionCount = 0; }
      if (isNaN(actionLimit)) { actionLimit = 0; }

      if (actionLimit > 0 && actionCount >= actionLimit) {
         return;
      }

      actionCount++;

      var actionTimeObject = new Date();
      actionTimeObject = Date.parse('+' + actionTimeout);

      ActionPopUpCookie.set("actionCount", actionCount, actionTimeObject);

      try {
         if (actionColor == "transparent") {
            //jQuery("#actionBlanket").hide();
            jQuery("#actionPopup")
               .css({opacity:0, display:"block"})
               .show()
               .scrollFollow({ offset:topValue, easing:"easeOutBack" })
               .animate({opacity:1.0}, 500)
            ;
         }
         else {
            jQuery("#actionBlanket")
               .css({opacity:0})
               .show()
               .animate({opacity:0.7}, 500, function() {
                  jQuery("#actionPopup").show().scrollFollow({ offset:topValue });
               })
            ;

            if (actionShowClose) { jQuery("#actionBlanket").click(function() { ActionPopup.hide(); }); }
         }

         if (!jQuery("#actionPopup").css("border")) {
            //jQuery("#actionPopup").css("border", "solid 3px");
         }
      }
      catch(_) { }

      ActionPopup.connect(true);
   },

   connect : function(shouldFocus) {
      if (action !== null && typeof action == "function") {
         action();
      }

      countdownObject = document.getElementById("actionCountdown");
      if (countdownObject) {
         actionTimer = setInterval(function() { ActionPopUp.countdown(countdownObject); }, 1000);
      }

      if (form == null || form == undefined) { return false; }

      // Set form submit behavior
      if (form.onsubmit) {
         var theForm = form;
         var oldSubmit = form.onsubmit;

         form.onsubmit = function() {
            if (oldSubmit() !== false) {
               activateForm(theForm);
            }
         };
      }
      else {
         form.onsubmit = function() { activateForm(form); };
      }

      if (shouldFocus !== undefined && shouldFocus == true) {
         jQuery(form).find(':input:visible:enabled:first').focus();  
      }
   },

   // Immediately hide the popup
   hide : function() {
      if (actionTimer !== null) { clearInterval(actionTimer); actionTimer = null; }

      try { jQuery("#actionBlanket").fadeOut(500); } catch(_) { }
      jQuery("#actionPopup").fadeOut(500, function() {
         jQuery("#actionHover").hide();

         if (Link.to !== null) {
            document.location = Link.to;
         }
      });
   },

   disable : function() {
      ActionPopup.shown = true;
   }
};

// Cookie functions
var ActionPopUpCookie = {

   set : function(name, value, days) {
      var date = new Date();
      if (days !== undefined && typeof days == "object") {
         date = days;
      }
      else {

      // Default to a 1 year cookie
      if (days === undefined) { days = 365; }

      // Format date string
      var date = new Date();
      date.setTime(date.getTime() + (days * 86400000));

      }

      // Set cookie name, value, and expiration date
      //window.status = name + "=" + value + "; expires=" + date.toGMTString() + "; path=/";
      document.cookie = name + "=" + value + "; expires=" + date.toGMTString() + "; path=/";
   },

   get : function(name) {
      // Find the cookie's value in the document cookie string
      var results = document.cookie.match(
         new RegExp("(?:^|; )" + name + "=" + "(.*?)(?:$|;)")
      );

      // Return the value if a match was found, undefined otherwise
      if (results && results.length > 1) { return results[1]; }
      return undefined;
   },

   clear : function(name) {
      // Erase a cookie
      ActionPopUpCookie.set(name, "", -1);
   },

   countdown : function(obj) {
      var start = parseInt(obj.innerHTML, 10);
      if (isNaN(start)) { return false; }

      start--;
      obj.innerHTML = start;

      if (start == 0) {
         ActionPopup.hide();
      }
   }

};

var Cursor = {
   x : null,
   y : null,

   lastX : null,
   lastY : null,

   archive : function() {
	   Cursor.lastX = Cursor.x;
	   Cursor.lastY = Cursor.y;
   },

   getCursor : function(e) {
       e = e ? e:event;

      if (e !== undefined && e.pageX && e.pageY) {
		   Cursor.archive();
         Cursor.x = parseInt(e.pageX, 10);
         Cursor.y = parseInt(e.pageY, 10);
      }
      else if (e && e.clientX && e.clientY) {
         Cursor.archive();
         Cursor.x = parseInt(e.clientX + document.body.scrollLeft, 10);
         Cursor.y = parseInt(e.clientY + document.body.scrollTop, 10);
      }
   }
};

var lastY = null;

// Function that will update the screen based on our mouse event
function handleMove(e) {
   // If the current Y coordinate is less than (at a higher position)
   // than the previous coordinate, the cursor is moving up
   if (e.clientY < lastY && e.clientY <= 10) {
      ActionPopup.show();
   }

   lastY = e.clientY;
}

function activateForm(form) {

   var validate = true;

   jQuery(form).find("input:text:visible:enabled").each(function() {
      if (jQuery(this).attr("value") == "") {
         validate = false;

         alert("Please fill in all fields!");
         jQuery(this).focus();
         return false;
      }

      else if (jQuery(this).hasClass("email") && !jQuery(this).attr("value").match(/@/)) {
         validate = false;

         alert("Please enter an e-mail address...");
         jQuery(this).focus();
         return false;
      }
   });

   if (validate == false) {
      return false;
   }

   ActionPopup.active = true;

   for (i=0;i<form.elements.length;i++) {
      var element = form.elements[i];

      if (element.type == 'text') {
         // Skip this text field if it contains an e-mail address.
         if (element.value.match(/@/)) { continue; }
         activate(element);
         break;
      }
   }
}

function activate(obj) {
   var firstname = obj.value;
   firstname = firstname.replace(/[^A-Z]/gi, "");
   firstname = firstname.replace(/ .*$/g, "");

   if (firstname != "") {   
      // Set the first name as a cookie
      ActionPopUpCookie.set("firstname", firstname);
      ActionPopup.brand("firstname", firstname);
   }

   if (Link.to !== null) { actionRedirect = Link.to; }

   if (actionRedirect != "") {
      window.location = actionRedirect;
   }
   else {
      ActionPopup.hide();
   }
}

myDomain = myDomain.replace(domainReplace, "");
yourDomain = yourDomain.replace(domainReplace, "");

var container;

var hop; // JV Plus
var form = null;
var action = function() { };

var loaded = false;

jQuery.browser.msie6 = jQuery.browser.msie && (jQuery.browser.version && jQuery.browser.version < 7 || /6.0/.test(navigator.userAgent));

jQuery(function() {

   jQuery("body")
      .append('<iframe name="catcher" id="catcher" width="1" height="1" style="display:none;" onload="if (ActionPopup.active) { ActionPopup.hide(); ActionPopup.thanks(); }"></iframe>')
      .append('<link rel="stylesheet" href="<?php echo getURL() ?>/actionpopup.php?css=' + escape(actionColor) + '" />')
   ;

   if (myDomain == yourDomain) {
      jQuery("body")
         .append(popCode + '<div id="actionBlanket"></div>')
      ;
   }

   jQuery("#actionBlanket").hide().css({
      backgroundColor:actionColor,
      width:"100%",
      height:"100%",
      top:0,
      left:0,

      zIndex:100
   });

   if (jQuery.browser.msie6) {
      jQuery("#actionBlanket")
         .css({ position:"absolute", zIndex:100, top: 0, left:0, width:"100%", height:jQuery(document).height() })
      ;
   }

   else {
      jQuery("#actionBlanket")
         .css({ position:"absolute", zIndex:100, top: 0, left:0, width:"100%", height:"100%" })
         .css({ position:"fixed" })
      ;
   }

   

   //if (!jQuery("#actionPopup").css("border")) {
   <?php if (eregi("^130.17", $_SERVER["REMOTE_ADDR"])): ?>
   //alert(jQuery("#actionPopup").css("border"));
   <?php endif; ?>

      jQuery("#actionPopup").hide().css({
         position:"absolute",
         top: topValue,
         left: leftValue,

         backgroundColor:"white",
         padding:10,
         zIndex:10000,
         verticalAlign: "middle"
      });
     
   //}

   //form = document.getElementById("actionPopup").getElementsByTagName("form")[0];
   try { form = jQuery("#actionPopup").find("form").get(0); }
   catch(_) { }

   jQuery("#actionPopup").find("form:first").attr("target", "catcher");

   if (document.getElementById("action-disable") || document.getElementById("GB_window")) {
      Link.disable();
   }
   else if (ActionPopUpCookie.get("firstname")) {
      // Brand the sales page contents with their first name
      ActionPopup.brand("firstname", ActionPopUpCookie.get("firstname"));
      Link.disable();
   }
   else {
      setTimeout("ActionPopup.init()", actionDelay*1000);
   }

});

<?php endif; ?>