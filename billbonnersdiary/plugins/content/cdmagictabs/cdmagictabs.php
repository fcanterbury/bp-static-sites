<?php
/**
 * Core Design Magic Tabs plugin for Joomla! 1.7
 * @author      Daniel Rataj, <info@greatjoomla.com>
 * @package     Joomla
 * @subpackage	Content
 * @category    Plugin
 * @version     2.0.2
 * @copyright	Copyright (C) 2007 - 2011 Great Joomla!, http://www.greatjoomla.com
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL 3
 * 
 * This file is part of Great Joomla! extension.   
 * This extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// no direct access
defined('_JEXEC') or die;

// Import library dependencies
jimport('joomla.plugin.plugin');

class plgContentCdMagicTabs extends JPlugin
{
	
	private	$JScriptegrator = 	null;
	private	$livepath		=	'';
	
	/**
	 * Constructor
	 *
	 * @access      protected
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 * @since       1.5
	 */
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);

		// define language
		$this->loadLanguage();
		
		$this->livepath = JURI::root(true) . '/';
	}

	/**
     * Call onContentPrepare function
     * Method is called by the view.
	 *
	 * @param	string	The context of the content being passed to the plugin.
	 * @param	object	The content object.  Note $article->text is also available
	 * @param	object	The content params
	 * @param	int		The 'page' number
	 * @since	1.6
	 */
	function onContentPrepare($context, &$article, &$params, $page = 0)
	{
		// regular expression
		$regex = "#{magictabs(?:\s?(.*?)?)?}(.*?){/magictabs}#is";

		if ( strpos( $article->text, '{magictabs' ) === false ) return false;
		
		// Scriptegrator check
		if (!class_exists('JScriptegrator')) {
			JError::raiseNotice('', JText::_('PLG_CONTENT_CDMAGICTABS_ENABLE_SCRIPTEGRATOR'));
			return false;
		} else {
			$this->JScriptegrator = JScriptegrator::getInstance('2.1.0');
			if ($error = $this->JScriptegrator->getError())  {
			   JError::raiseNotice('', $error);
			   return false;
      		}
		}

		$document = JFactory::getDocument(); // set document for next usage
		
		// add general stylesheet
		$document->addStyleSheet($this->livepath . 'plugins/content/' . $this->_name . '/css/' . $this->_name . '.css', 'text/css');
		
		// Explication:
		// $match[1]	-> parameters
		// $match[2]	-> text to tabs

		// replacement regular expression
		$article->text = preg_replace_callback($regex, array($this, 'replacer'), $article->text);
		
		// check buffer
		if ( $article->text === null ) {
			switch ( preg_last_error() ) {
				case PREG_BACKTRACK_LIMIT_ERROR:
					$message = "PHP regular expression limit reached (pcre.backtrack_limit)";
					break;
				case PREG_RECURSION_LIMIT_ERROR:
					$message = "PHP regular expression limit reached (pcre.recursion_limit)";
					break;
				case PREG_BAD_UTF8_ERROR:
					$message = "Bad UTF8 passed to PCRE function";
					break;
				default:
					$message = "Unknown PCRE error calling PCRE function";
			}
			JError::raiseError( 500, $message );
			return false;
		}
	}

	/**
	 * Plugin replacer
	 *
	 * @return string
	 */
	function replacer(&$match)
	{
		$document = JFactory::getDocument();
		
		$default_animated = $this->params->get('default_animated', 'both');
		$tabwidth = $this->params->get('default_tabwidth', '80px');
		$default_boxwidth = $this->params->get('default_boxwidth', '0');
		$default_tabalign = $this->params->get('default_tabalign', 'left');
		$default_taboffset = $this->params->get('default_taboffset', '0px');
		$default_tabrotate = (int)$this->params->get('default_tabrotate', 0);
		$default_tabevent = $this->params->get('default_tabevent', 'click');
		$def_navigation = $this->params->get('navigation', 0);
		$readmore = $this->params->get('readmore', 0);

		// define regex parameters
		$tabs_settings = ( isset($match[1]) ? $match[1] : '' );
		$tabs_text = ( isset($match[2]) ? $match[2] : '' );

		unset($match);

		// Mode
		preg_match("#^[0-9:]*?$#", $tabs_text, $mode);
		$mode = (isset($mode[0]) ? 'id' : 'manually');
		
		// tab id
		preg_match('#id\s?=\s?"([0-9a-zA-Z]{1,})"#', $tabs_settings, $tab_id);
		$tab_id = (isset($tab_id[1]) ? $tab_id[1] : $this->random(5));

		// style
		$files = implode('|', $this->JScriptegrator->themeList());
		preg_match('#uitheme\s?=\s?"(' . $files . ')?"#', $tabs_settings, $uitheme);
		$uitheme = ( isset($uitheme[1]) ? $uitheme[1] : $this->params->get('uitheme', 'ui-lightness'));
		
		if ($uitheme) {
			$this->JScriptegrator->importLibrary(array(
				'jquery',
				'jqueryui' => array(
					'uitheme' => $uitheme
				)
			));
			if ($error = $this->JScriptegrator->getError()) {
				 JError::raiseNotice('', $error);
				 return false;
			}
		}
		
		// tab animated
		preg_match('#animation\s?=\s?"(both|slide|fade|none)"#', $tabs_settings, $animation);
		$animation = ( isset($animation[1]) ? $animation[1] : $default_animated);
		
		switch ($animation)
		{
			case 'none':
				$animation = null;
				break;
			case 'both':
				$animation = 'fx: { height: \'toggle\', opacity: \'toggle\' }';
				break;
			case 'slide':
				$animation = 'fx: { height: \'toggle\' }';
				break;
			case 'fade':
				$animation = 'fx: { opacity: \'toggle\' }';
				break;
			default:
				$animation = null;
				break;
		}
		// end


		// box width
		preg_match('#boxwidth\s?=\s?"([0-9empx%.]{1,})"#', $tabs_settings, $tab_boxwidth);
		$tab_boxwidth = (isset($tab_boxwidth[1]) ? $tab_boxwidth[1] : $default_boxwidth);
		
		// tab align (float - left, right)
		preg_match('#tabalign\s?=\s?"(left|right)"#', $tabs_settings, $tab_align);
		$tab_align = (isset($tab_align[1]) ? $tab_align[1] : $default_tabalign);
		
		switch ($tab_align)
		{
			case 'left':
				$tab_align = 'left';
				break;
			case 'right':
				$tab_align = 'right';
				break;
			default:
				break;
		}
		// end

		// tab offset (padding-left)
		preg_match('#offset\s?=\s?"([0-9empx%.]{1,})"#', $tabs_settings, $tab_offset);
		$tab_offset = (isset($tab_offset[1]) ? $tab_offset[1] : $default_taboffset);
		
		// tab select
		preg_match('#select\s?=\s?"([1-9]{1,})"#', $tabs_settings, $tab_select);
		$tab_select = (isset($tab_select[1]) ? 'selected: ' . --$tab_select[1] : null);
		
		// event
		preg_match('#event\s?=\s?"(click|mouseover)"#', $tabs_settings, $tab_event);
		$tab_event = (isset($tab_event[1]) ? $tab_event[1] : $default_tabevent);
		
		switch ($tab_event)
		{
			case 'click':
				$tab_event = 'click';
				break;
			case 'mouseover':
				$tab_event = 'mouseover';
				break;
			default:
				$tab_event = 'click';
				break;
		}

		$tab_event = "event: '$tab_event'";
		// end

		if ($tab_event and $animation and $tab_select)
		{
			$settings = $tab_event . ', ' . $animation . ', ' . $tab_select;
		} elseif ($tab_event and $animation)
		{
			$settings = $tab_event . ', ' . $animation;
		} elseif ($tab_event and $tab_select)
		{
			$settings = $tab_event . ', ' . $tab_select;
		} else
		{
			$settings = $tab_event;
		}

                // tab rotate
		preg_match('#rotate\s?=\s?"([0-9]{1,})"#', $tabs_settings, $tab_rotate);
		$tab_rotate = (isset($tab_rotate[1]) ? $tab_rotate[1] : $default_tabrotate);

		if ($tab_rotate) {
			$tab_rotate = '.tabs(\'rotate\', ' . $tab_rotate . ')';
		} else {
			$tab_rotate = '';
		}
		
		// tab navigation
		preg_match('#navigation\s?=\s?"(yes|no)"#', $tabs_settings, $navigation);
		$navigation = ( isset($navigation[1]) ? $navigation[1] : '');
		
		if ($navigation) {
			$navigation = ($navigation == 'yes' ? 1 : 0);
		} else {
			$navigation = $def_navigation;
		}
		
		$custom_setting_js = "
		<!--
		jQuery(function($) {
			// create tabs instance
			$('#magictabs_$tab_id').tabs({ $settings })$tab_rotate;
		";
		
		// prev next links script
		if ($navigation) {
			$custom_setting_js .= "
			// prev - next buttons
			var navigation_links = $('.cdmagictabs_navigation a', '#magictabs_$tab_id');
			navigation_links.button({ text: false });
			
			navigation_links.each(function(n, el) {
				if ($(this).hasClass('prev')) {
					$(this).button('option', { icons : { primary : 'ui-icon-circle-arrow-w' } });
				}
				if ($(this).hasClass('next')) {
					$(this).button('option', { icons : { primary : 'ui-icon-circle-arrow-e' } });
				}
			
				$(this).click(function (){
					var tabs_instance = $(this).closest('#magictabs_$tab_id'),
					selected = tabs_instance.tabs('option', 'selected');
					
					" . ($tab_align == "left" ? "var count = selected + 1;" : "var count = selected - 1;") . "
					" . ($tab_align == "left" ? "if ($(el).hasClass('prev')) count = selected - 1;" : "if ($(el).hasClass('prev')) count = selected + 1;") . "
					tabs_instance.tabs('select', count);
					return false;
				});
			});
			";
		}
		
		$custom_setting_js .= "
		});
		//-->
		";
		
		$document->addScriptDeclaration($custom_setting_js);

		$this->customScript($tab_id, $tab_boxwidth, $tab_align, $tab_offset); // load custom CSS
		// end

		
		$tabs = new stdClass();
		$tabs->id = $tab_id;
		
		switch ($mode)
		{

			case 'id':
					
				$tabs_text = trim(strip_tags($tabs_text)); // prevent HTML characters in article ids
					
				if (explode(':', $tabs_text))
				{
					$set_articles = explode(':', $tabs_text);
				} else
				{
					return;
				}
				
				// article routing
				require_once (JPATH_SITE . DS . 'components' . DS . 'com_content' . DS . 'helpers' . DS . 'route.php');

				foreach ($set_articles as $key => $item_id)
				{
					$item = $this->getItem($item_id);
					
					// some error
					if (!is_object($item)) continue;
					
					// check view access
					if (!$item->params->get('access-view')) {
						continue;
					}
					
					$tabs_text = new stdClass();
					$tabs_text->text = '';
					$tabs_text->readmore_link = '';
					$tabs_text->navigation = '';
					
					$key = ++$key;
					
					$title = strip_tags($item->title);
					$fulltext = $item->fulltext;
					$text = ($item->introtext ? $item->introtext : $fulltext);
						
					$atitle = ( $title ? $title : '' );
					
					$tabs_header = new stdClass();
					$tabs_header->atitle = $atitle;
					$tabs->header[] = $tabs_header;
					
					$text = ( $text ? $text : '' );
					
					$prevlink = '<a href="#magictabs_' . $tab_id . '_' . ($key - 1) . '" class"prev" title="' . JText::_('PLG_CONTENT_CDMAGICTABS_PREVIOUS') . '">' . JText::_('PLG_CONTENT_CDMAGICTABS_PREVIOUS') . '</a>';
					$nextlink = '<a href="#magictabs_' . $tab_id . '_' . ($key + 1) . '" class="next" title="' . JText::_('PLG_CONTENT_CDMAGICTABS_NEXT') . '">' . JText::_('PLG_CONTENT_CDMAGICTABS_NEXT') . '</a>';
					
					$links = '<div class="cdmagictabs_navigation">';
		
					if ($tab_align == 'left') {
						if ($key + 1 <= count($set_articles)) $links .= $nextlink;
						$links .= ' ';
						if ($key - 1 !== 0) $links .= $prevlink;
						
					} else {
						// right
						$key = count($set_articles) - $key + 1;
						if ($key + 1 <= count($set_articles)) $links .= $nextlink;
						$links .= ' ';
						if ($key - 1 !== 0) $links .= $prevlink;
					}
					
					$links .= '</div>'; 
					
					// no navigation, no links
					if (!$navigation) $links = '';
					
					$tabs_text->text = $text;
					
					if ($fulltext and $readmore)
					{
						// Add router helpers.
						$item->slug			= $item->alias ? ($item->id.':'.$item->alias) : $item->id;
						$item->catslug		= $item->category_alias ? ($item->catid.':'.$item->category_alias) : $item->catid;
						$item->parent_slug	= $item->category_alias ? ($item->parent_id.':'.$item->parent_alias) : $item->parent_id;
						
						$tabs_text->readmore_link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catslug));
					}
					
					$tabs_text->navigation = $links;
					
					$tabs->text[] = $tabs_text;
				}
				
				break;
				
			case 'manually':
				
				if (preg_split('#\|\|\|\|#', $tabs_text, -1, PREG_SPLIT_NO_EMPTY))
				{
					$tabs_text_array = preg_split('#\|\|\|\|#', $tabs_text, -1, PREG_SPLIT_NO_EMPTY);
				} else
				{
					return '';
				}

				foreach ($tabs_text_array as $key=>$text)
				{
					$tabs_text = new stdClass();
					$tabs_text->text = '';
					$tabs_text->readmore_link = '';
					$tabs_text->navigation = '';
					
					$tabs_array = explode('::', $text);

					$atitle = ( isset($tabs_array[0]) ? trim(strip_tags($tabs_array[0])) : '' );
					$tabs_text->text = ( isset($tabs_array[1]) ? $tabs_array[1] : '' );
					
					$tabs_header = new stdClass();
					$tabs_header->atitle = $atitle;
					$tabs->header[] = $tabs_header;
					
					// navigation
					if ($navigation) {
						$key = ++$key;
						
						$prevlink = '<a href="#magictabs_' . $tab_id . '_' . ($key - 1) . '" class="prev" title="' . JText::_('PLG_CONTENT_CDMAGICTABS_PREVIOUS') . '">' . JText::_('PLG_CONTENT_CDMAGICTABS_PREVIOUS') . '</a>';
						$nextlink = '<a href="#magictabs_' . $tab_id . '_' . ($key + 1) . '" class="next" title="' . JText::_('PLG_CONTENT_CDMAGICTABS_NEXT') . '">' . JText::_('PLG_CONTENT_CDMAGICTABS_NEXT') . '</a>';
						
						$tabs_text->navigation = '<div class="cdmagictabs_navigation">';
						
						if ($tab_align == 'left') {
							if ($key + 1 <= count($tabs_text_array)) {
								$tabs_text->navigation .= $nextlink;
							}
							$tabs_text->navigation .= ' ';
							if ($key - 1 !== 0) {
								$tabs_text->navigation .= $prevlink;
							}
							
						} else {
							// right
							$key = count($tabs_text_array) - $key + 1;
							if ($key + 1 <= count($tabs_text_array)) {
								$tabs_text->navigation .= $nextlink;
							}
							$tabs_text->navigation .= ' ';
							if ($key - 1 !== 0) {
								$tabs_text->navigation .= $prevlink;
							}
						}
						
						$tabs_text->navigation .= '</div>'; 
					// end
					}
					
					$tabs->text[] = $tabs_text;
				}
				
				break;
			default: return; break;
		}
		
		if (!$layoutpath = $this->getLayoutPath('view.html')) return false;
		
		$html = '';
		ob_start();
			require($layoutpath);
			$html .= ob_get_contents();
		ob_end_clean();
		
		return $html;

	}

	/**
	 * Create a Random String
	 *
	 * @param $length
	 * @return string		Generated hash.
	 */
	function random($length = 5)
	{
		$alphanum = 'abcdefghijklmnopqrstuvwxyz';
		$var_random = '';
		mt_srand(10000000 * (double)microtime());
		for ($i = 0; $i < (int)$length; $i++)
		$var_random .= $alphanum[mt_rand(0, 25)];
		return $var_random;
	}

	/**
	 * Create custom script and style
	 *
	 * @param $set_id
	 * @param $tab_style
	 * @param $boxwidth
	 * @param $tab_align
	 * @param $offset
	 * @return void
	 */
	function customScript($set_id, $boxwidth = '0', $tab_align =
        'left', $offset = '0px') {

		$document = JFactory::getDocument();
		$tab_offset = '';
		switch ($tab_align)
		{
			case 'left':
				$tab_offset = '';
				break;
			case 'right':
				$tab_offset = "$('#magictabs_$set_id > ul').css({ \"padding-right\" : \"$offset\" });";
				break;
			default:
				break;
		}
	
		$custom_css = "	#magictabs_$set_id .ui-tabs-nav li { float: $tab_align; }";
	
		$document->addStyleDeclaration($custom_css);
		
		if ($boxwidth) {
			$boxwidth = "$(\"#magictabs_$set_id\").css({ \"width\" : \"$boxwidth\" });";
		} else {
			$boxwidth = '';
		}
	
		if ($tab_offset or $boxwidth) {
			$js = "
			<!--
			jQuery(document).ready(function($){
				$tab_offset $boxwidth
			});
			//-->";
					
			$document->addScriptDeclaration($js);
		}
     }
     
     
     	/**
         * Load article
         * 
         * @param $id
         * @return object
         */
        function getItem($aid = 0)
        {
        	$application = JFactory::getApplication();
        	
        	jimport('joomla.application.component.model');
        	$model = JModel::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
        	
        	$params = $application->getParams();
			$model->setState('params', $params);
        	
        	// Initialise variables.
			$aid = (!empty($aid)) ? $aid : 0;
	
			if (!isset($item) or $item === null) {
				$item = array();
			}
	
			if (!isset($item[$aid])) {
	
				try {
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
	
					$query->select($model->getState(
						'item.select', 'a.id, a.asset_id, a.title, a.alias, a.title_alias, a.introtext, a.fulltext, ' .
						// If badcats is not null, this means that the article is inside an unpublished category
						// In this case, the state is set to 0 to indicate Unpublished (even if the article state is Published)
						'CASE WHEN badcats.id is null THEN a.state ELSE 0 END AS state, ' .
						'a.mask, a.catid, a.created, a.created_by, a.created_by_alias, ' .
						'a.modified, a.modified_by, a.checked_out, a.checked_out_time, a.publish_up, a.publish_down, ' .
						'a.images, a.urls, a.attribs, a.version, a.parentid, a.ordering, ' .
						'a.metakey, a.metadesc, a.access, a.hits, a.metadata, a.featured, a.language, a.xreference'
						)
					);
					$query->from('#__content AS a');
	
					// Join on category table.
					$query->select('c.title AS category_title, c.alias AS category_alias, c.access AS category_access');
					$query->join('LEFT', '#__categories AS c on c.id = a.catid');
	
					// Join on user table.
					$query->select('u.name AS author');
					$query->join('LEFT', '#__users AS u on u.id = a.created_by');
	
					// Join on contact table
					$query->select('contact.id as contactid' ) ;
					$query->join('LEFT','#__contact_details AS contact on contact.user_id = a.created_by');
					
					
					// Join over the categories to get parent category titles
					$query->select('parent.title as parent_title, parent.id as parent_id, parent.path as parent_route, parent.alias as parent_alias');
					$query->join('LEFT', '#__categories as parent ON parent.id = c.parent_id');
	
					// Join on voting table
					$query->select('ROUND( v.rating_sum / v.rating_count ) AS rating, v.rating_count as rating_count');
					$query->join('LEFT', '#__content_rating AS v ON a.id = v.content_id');
	
					$query->where('a.id = ' . (int) $aid);
	
					// Filter by start and end dates.
					$nullDate = $db->Quote($db->getNullDate());
					$nowDate = $db->Quote(JFactory::getDate()->toMySQL());
	
					$query->where('(a.publish_up = ' . $nullDate . ' OR a.publish_up <= ' . $nowDate . ')');
					$query->where('(a.publish_down = ' . $nullDate . ' OR a.publish_down >= ' . $nowDate . ')');
	
					// Join to check for category published state in parent categories up the tree
					// If all categories are published, badcats.id will be null, and we just use the article state
					$subquery = ' (SELECT cat.id as id FROM #__categories AS cat JOIN #__categories AS parent ';
					$subquery .= 'ON cat.lft BETWEEN parent.lft AND parent.rgt ';
					$subquery .= 'WHERE parent.extension = ' . $db->quote('com_content');
					$subquery .= ' AND parent.published <= 0 GROUP BY cat.id)';
					$query->join('LEFT OUTER', $subquery . ' AS badcats ON badcats.id = c.id');
					
					$published = 1;
					$archived = 2;
					
					if (is_numeric($published)) {
						$query->where('(a.state = ' . (int) $published . ' OR a.state =' . (int) $archived . ')');
					}
					
					$db->setQuery($query);
	
					$data = $db->loadObject();
					
					if ($error = $db->getErrorMsg()) {
						throw new Exception($error);
					}
	
					if (empty($data)) {
						throw new JException(JText::_('COM_CONTENT_ERROR_ARTICLE_NOT_FOUND'), 404);
					}
					
					// Check for published state if filter set.
					if (((is_numeric($published)) || (is_numeric($archived))) && (($data->state != $published) && ($data->state != $archived))) {
						throw new JException(JText::_('COM_CONTENT_ERROR_ARTICLE_NOT_FOUND'), 404);
					}
					
					// Convert parameter fields to objects.
					$registry = new JRegistry;
					$registry->loadString($data->attribs);
					$data->params = clone $model->getState('params');
					$data->params->merge($registry);
	
					$registry = new JRegistry;
					$registry->loadString($data->metadata);
					$data->metadata = $registry;
	
					// Compute selected asset permissions.
					$user	= JFactory::getUser();
	
					// Technically guest could edit an article, but lets not check that to improve performance a little.
					if (!$user->get('guest')) {
						$userId	= $user->get('id');
						$asset	= 'com_content.article.'.$data->id;
	
						// Check general edit permission first.
						if ($user->authorise('core.edit', $asset)) {
							$data->params->set('access-edit', true);
						}
						// Now check if edit.own is available.
						else if (!empty($userId) && $user->authorise('core.edit.own', $asset)) {
							// Check for a valid user and that they are the owner.
							if ($userId == $data->created_by) {
								$data->params->set('access-edit', true);
							}
						}
					}
	
					// Compute view access permissions.
					if ($access = $model->getState('filter.access')) {
						// If the access filter has been set, we already know this user can view.
						$data->params->set('access-view', true);
					}
					else {
						// If no access filter is set, the layout takes some responsibility for display of limited information.
						$user = JFactory::getUser();
						$groups = $user->getAuthorisedViewLevels();
	
						if ($data->catid == 0 || $data->category_access === null) {
							$data->params->set('access-view', in_array($data->access, $groups));
						}
						else {
							$data->params->set('access-view', in_array($data->access, $groups) && in_array($data->category_access, $groups));
						}
					}
	
					$item[$aid] = $data;
				}
				catch (JException $e)
				{
					$this->setError($e);
					$item[$aid] = false;
				}
			}
			
			return $item[$aid];
        }

	/**
     * Get Layout
     * 
     * @param $layout
     * @return string
     */
    function getLayoutPath($layout = '') {
    	if (!$layout) return false;
    	
    	$filepath = dirname(__FILE__) . DS . 'tmpl' . DS . $layout . '.php';
    	if (!JFile::exists($filepath)) return false;
		
		return $filepath;
    }
    
	/**
	 * Clean content from "ob_get_contents()" function
	 * 
	 * @param	string	$string
	 * @return 	string
	 */
	private function cleanObGetContents($string = '')
	{
		return preg_replace( '#\t?|\n?#' , '', $string );
	}

}
?>