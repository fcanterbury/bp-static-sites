<?php
/**
 * Core Design Magic Tabs plugin for Joomla! 1.7
 * @author      Daniel Rataj, <info@greatjoomla.com>
 * @package     Joomla
 * @subpackage	Content
 * @category    Plugin
 * @version     2.0.2
 * @copyright	Copyright (C) 2007 - 2011 Great Joomla!, http://www.greatjoomla.com
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL 3
 * 
 * This file is part of Great Joomla! extension.   
 * This extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// no direct access
defined('_JEXEC') or die;
?>
<div class="<?php echo $uitheme; ?>">
	<div id="magictabs_<?php echo $tab_id; ?>">
		<ul>
			<?php foreach($tabs->header as $key=>$tab_header): ?>
				<li><a href="#magictabs_<?php echo $tabs->id; ?>_<?php echo ++$key; ?>"><?php echo $tab_header->atitle; ?></a></li>
			<?php endforeach; ?>
		</ul>
		<?php foreach($tabs->text as $key=>$tab_text): ?>
			<div id="magictabs_<?php echo $tabs->id; ?>_<?php echo ++$key; ?>">
				<?php echo $tab_text->text; ?>
				<?php if ($tab_text->readmore_link): ?>
					<div class="cdmagictabs_readmore">
						<a class="readmore" href="<?php echo $tab_text->readmore_link; ?>" title="<?php echo JText::_('PLG_CONTENT_CDMAGICTABS_READ_MORE_LINK'); ?>">
							<?php echo JText::_('PLG_CONTENT_CDMAGICTABS_READ_MORE_LINK'); ?>
						</a>
					</div>
				<?php endif; ?>
				<?php if ($tab_text->navigation): ?>
					<div style="clear: both;"></div>
					<?php echo $tab_text->navigation; ?>
					<div style="clear: both;"></div>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	</div>
</div>