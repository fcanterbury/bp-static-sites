<?php

/**
 * @version 1.0
 * @package GITags
 * @subpackage plugin
 * @author Gabriel Moise (gabi.moise@gmail.com)
 * @copyright (C) 2011 by Gabriel Moise (http://www.gi-news.com)
 * @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html
 *
 * If you fork this to create your own project,
 * please make a reference to GITags someplace in your code
 * and provide a link to http://www.gi-news.com
 **/



/* Check to ensure this file is included in Joomla! */
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.html.parameter' ); 


class plgContentGITags extends JPlugin {
	
	function plgContentGITags(&$subject, $params)	{
		parent::__construct($subject, $params);
	}


	/**
	* Retrieves tags list
	*
	* @param 	object		The article object.  Note $article->text is also available
	* @param 	object		The article params
	* @param 	int		The 'page' number
	* @return	string		Tags list to be displayed
	*/
        function onContentPrepare( $context, &$article, &$params, $page = 0 )
        
        {
	
            $option = JRequest::getCmd( 'option' , 'null');
            $view = JRequest::getCmd('view', 'null');
            $layout = JRequest::getCmd('layout', 'null');

            $css = JURI::base().'components'.DS.'com_gitags'.DS.'style.css';
            $document =& JFactory::getDocument();
            $document->addStyleSheet($css);

            $flag = 0;
            $text = '<p class="gitags">';

            $plugin =& JPluginHelper::getPlugin('content', 'gitags');
            $pluginParams = new JParameter($plugin->params);

            if ($pluginParams->get('title', 1)) $text .= JText::_('Tags'). ': ';

            $tags = array();
			if (!empty($article->id))
            	$tags = $this->contentGITags_getTags($option, $article->id);
            $separator = $pluginParams->get('separator', '|');
            $attribs['rel'] = "tag nofollow";

            @include( 'administrator'.DS.'components'.DS.'com_sh404sef'.DS.'config'.DS.'config.sef.php' );
            
            $mainframe = JFactory::getApplication();
            $Enabled = 1;
            if ($Enabled==1) $com = "com_search";
            else
            {
                if ($mainframe->getCfg('sef') == 1)
                    $com = "com_gitags";
                else $com = "com_search";
            }


            foreach($tags as $tag)
            {
                    $formattedName = str_replace(" ", "+", substr($tag->name, 0, 20));
                    if ($flag == 0) $flag = 1;
                            else $text .= " ".$separator." ";
                    $link = JRoute::_('index.php?option='.$com.'&searchword=' .
                                                    trim($formattedName) . '&areas[]=gitags', $attribs);
                    $link = str_replace('%252F', '', $link);//router bug
                    $text .= JHTML::link($link, $tag->name);
            }

            $text .= '</p>';

            $blogpages = $pluginParams->get('blogs', 1);
            $overviewpage = 0;
            if ( ($option=="com_content" && $view=="frontpage") ||
                            ($option=="com_content" && ($view=="category" || $layout=="blog")) ||
                            ($option=="com_content" && ($view=="section" && $layout=="blog"))
            )
            {
                    $overviewpage = 1;
            }

            if ( ( $overviewpage == 1 && $blogpages == 1 ) || $overviewpage == 0 )
            {
                    if ($tags)
                    {
                            if ($pluginParams->get('position', 1))
                                            $article->text = $text.$article->text;
                            else $article->text .= $text;
                    }
                    else return;
            }
            else
                    return;
        }

	/**
	* Retrieves tags list from database
	*
	* @access	public
	* @param 	Int		$id		Item ID
	* @param 	String		$option		component
	* @return 	String		$tags		Array of tags
	* @since	1.0
	*/
        function contentGITags_getTags($option, $id)
        {
                $tags = array();

                $db =& JFactory::getDBO();
                $query = 'SELECT * FROM #__gitags_tags NATURAL JOIN #__gitags_items' .
                                 ' WHERE component = \''.$option.'\' AND item_id = \''.$id.'\' ORDER BY `name`';
                $db->setQuery($query);
                $tags = $db->loadObjectList();

                return $tags;
        }

}
?>