<?php
/**
 * GiTags Save Plugin
 *
 * @version 1.0
 * @package gitube
 * @author Gabriel Moise
 * @copyright Copyright (C) 2011 Gabriel Moise. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * based on JTags : http://extensions.joomla.org/extensions/search-a-indexing/tags-a-clouds/5728 developed by Jacek Zielinski
 */

/* Check to ensure this file is included in Joomla! */
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.html.parameter' );

class plgContentGiTagsSave extends JPlugin
{
    function plgSystemGiTagsSave(& $subject, $config)
	{
		parent::__construct($subject, $config);
	}
        	
    
    /**
	* Saves tags into database
	* Method is called right after the content is saved
	*
	* @param 	object		A JTableContent object
	* @param 	bool		If the content is just about to be created
	*/
	//function onAfterContentSave( &$article, $isNew )
        public         function onContentAfterSave($context, &$article, $isNew) 
	{
			$tagsList = JRequest::getVar('tags');
			$tagsList = str_replace(", ", ",", $tagsList);
			$tagsList = str_replace(" ,", ",", $tagsList);

			$tagsArray = explode(',', $tagsList);

			if ($tagsList != "")
			{
				/* check if all tags have more or equal 3 characters */
                       $plugin =& JPluginHelper::getPlugin('search', 'gitags');
                        $pluginParams = new JParameter( $plugin->params );
                        $charCount = (int)$pluginParams->get( 'char_count', 3 );    
                            
				foreach ($tagsArray as $tag)
				{
					if (strlen($tag) < $charCount)
					{
						JError::raiseWarning( '1', JText::_("Tags should have at least $charCount characters. You can change this limit in <b>GiTags Search</b> plugin options !") );
						$error = true;
						break;
					}
				}

				/* if all tags are OK */
				if (!$error)
				{
					$id = JRequest::getVar('id');
					$db =& JFactory::getDBO();
					
					if (!$id)
					{
						$cid = JRequest::getVar( 'cid' , array() , '' , 'array' );
						$id = $cid[0];
					}
					if ($id == 0)
					{
						$query = 'SELECT LAST_INSERT_ID();';
					    $db->setQuery($query); 
					    $id = $db->loadResult(); 
					}
					
					$option = JRequest::getVar('option');
	
					$query = 'DELETE FROM #__gitags_items WHERE component = \''.$option.'\' and item_id = \''.$id.'\'';
					$db->setQuery( $query );
					if (!$db->query())
					{
						echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
					}
	
					/* if tags list is not empty */
					if ($tagsList != "")
					{
						foreach ($tagsArray as $tag)
						{
							$tag_id = $this->getTagId($tag);
							if (!isset($tag_id))
							{
								$this->insertTag($tag);
								$tag_id = $this->getTagId($tag);
							}
							$query = 'INSERT INTO #__gitags_items (`component`, `tag_id`, `item_id`) VALUES (\''.$option.'\', \''.$tag_id.'\', \''.$id.'\')';
							$db->setQuery( $query );
							if (!$db->query())
							{
								echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
							}
						}
					}
				}
			}
	}

	/**
	* Gets tag ID
	*
	* @param 	String		$title		tag name
	* @return 	String		$tagsList	tag id
	*/
	function getTagId($title)
	{
		$query = 'SELECT tag_id FROM #__gitags_tags WHERE name=\''.$title.'\'';
		$db =& JFactory::getDBO();
		$db->setQuery($query);
		return $db->loadResult();
	}

	/**
	* Inserts tag to database
	*
	* @param 	String		$title		tag name
	*/
	function insertTag($title)
	{
		$query = 'INSERT INTO #__gitags_tags (`name`) VALUES (\''.$title.'\')';
		$db =& JFactory::getDBO();
		$db->setQuery( $query );
		if (!$db->query())
		{
			echo "<script> alert('".$db->getErrorMsg()."'); window. history.go(-1); </script>\n";
		}
	}
}
?>