<?php
/**
* @package		Joomla
* @subpackage	System
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

/* Check to ensure this file is included in Joomla! */
defined( '_JEXEC' ) or die( 'Restricted access' );
$mainframe->registerEvent( 'onPrepareContent', 'pluginContentJTags' );

	/**
	* Retrieves tags list
	*
	* @param 	object		The article object.  Note $article->text is also available
	* @param 	object		The article params
	* @param 	int			The 'page' number
	* @return	string		Tags list to be displayed
	*/
function pluginContentJTags( &$article, &$params, $page=0 )
{
	global $mainframe;
	
	$option = JRequest::getCmd( 'option' , 'null');
	$view = JRequest::getCmd('view', 'null');
	$layout = JRequest::getCmd('layout', 'null');

	$css = JURI::base().'components/com_jtags/style.css';
	$document =& JFactory::getDocument();
	$document->addStyleSheet($css);

	$flag = 0;
	$text = '<p class="tags">';

	$plugin =& JPluginHelper::getPlugin('content', 'jtags');
	$pluginParams = new JParameter($plugin->params);

	if ($pluginParams->get('title', 1)) $text .= JText::_('Other Related Topics'). ': ';

	$tags = array();
	$tags = contentJTags_gettags($option, $article->id);
	$separator = $pluginParams->get('separator', '|');
	$attribs['rel'] = "tag nofollow";

	if ($mainframe->getCfg('sef') == 1)
		$com = "com_jtags";
	else $com = "com_search";

	foreach($tags as $tag)
	{
		$formattedName = str_replace(" ", "+", substr($tag->name, 0, 20));
		if ($flag == 0) $flag = 1;
			else $text .= " ".$separator." ";
		$link = JRoute::_('index.php?option='.$com.'&searchword=' .
						$formattedName . '&areas[]=jtags', $attribs);
		$text .= JHTML::link($link, $tag->name);
	}

	$text .= '</p>';

	$blogpages = $pluginParams->get('blogs', 1);
	$overviewpage = 0;
	if ( ($option=="com_content" && $view=="frontpage") ||
			($option=="com_content" && ($view=="category" || $layout=="blog")) ||
			($option=="com_content" && ($view=="section" && $layout=="blog"))
	)
	{
		$overviewpage = 1;
	}

	if (strpos($article->text, "{jtagstpg}")) {
	return;
	}
	
	if ( ( $overviewpage == 1 && $blogpages == 1 ) || $overviewpage == 0 )
	{
		if ($tags)
		{
			if ($pluginParams->get('position', 1))
					$article->text = $text.$article->text;
			else $article->text .= $text;
		}
		else return;
	}
	else
		return;
}

	/**
	* Retrieves tags list from database
	*
	* @access	public
	* @param 	Int			$id			Item ID
	* @param 	String		$option		component
	* @return 	String		$tags		Array of tags
	* @since	1.5
	*/
function contentJTags_gettags($option, $id)
{
	$tags = array();

	$db =& JFactory::getDBO();
	$query = 'SELECT * FROM #__jtags_tags NATURAL JOIN #__jtags_items' .
			 ' WHERE component = \''.$option.'\' AND item_id = \''.$id.'\' ORDER BY `name`';
	$db->setQuery($query);
	$tags = $db->loadObjectList();

	return $tags;
}
?>