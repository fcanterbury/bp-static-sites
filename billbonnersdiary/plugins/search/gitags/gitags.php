<?php

/**
 * GiTags Search Plugin
 *
 * @version 1.0
 * @package gitube
 * @author Gabriel Moise
 * @copyright Copyright (C) 2011 Gabriel Moise. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * based on JTags : http://extensions.joomla.org/extensions/search-a-indexing/tags-a-clouds/5728 developed by Jacek Zielinski
 */

/* Check to ensure this file is included in Joomla! */
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.html.parameter' );

/**
 * @return array An array of search areas
 */

class plgSearchGiTags extends JPlugin
{
    
    public function __construct(& $subject, $config)
    {
            parent::__construct($subject, $config);
            $this->loadLanguage();
            $lang = JFactory::getLanguage();
            $lang->setLowerLimitSearchWordCallback(array('plgSearchGiTags', 'charLimit'));
            $lang->setIgnoredSearchWordsCallback(array('plgSearchGiTags', 'useStopWords'));
    }
    
    
    function onContentSearchAreas() {
	static $areas = array(
		'gitags' => 'Tags'
	);
	return $areas;
    }
/**
* Tags Search method
*
* The sql must return the following fields that are used in a common display
* routine: href, title, section, created, text, browsernav
* @param string Target search string
* @param string matching option, exact|any|all
* @param string ordering option, newest|oldest|popular|alpha|category
*/
function onContentSearch($text, $phrase='', $ordering='', $areas=null)
{
	$mainframe = JFactory::getApplication();
	if (!$text) {
		return array();
	}
	require_once(JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');
	if (is_array($areas)) {
		if (!array_intersect( $areas, array_keys( $this->onContentSearchAreas() ) )) {
			return array();
		}
	}
	$plugin =& JPluginHelper::getPlugin('search', 'gitags');
 	$pluginParams = new JParameter( $plugin->params );
	$pluginParams->get( 'search_limit', 50 );$limit = 50;//

        $db		=& JFactory::getDBO();
	$user	=& JFactory::getUser();

	//$where = "(LOWER(name) REGEXP '(^|,)$text($|,)')";
        $where = "(LOWER(name) LIKE '%$text%')";

	switch ($ordering) {
		case 'oldest':
			$order = '#__gitags_items.item_id ASC';
			break;
		case 'alpha':
			$order = 'title ASC';
			break;
		case 'popular':
			$order = 'hits DESC';
			break;
		case 'category':
			$order = 'catid ASC';
			break;
		case 'newest':
		default:
			$order = '#__gitags_items.item_id DESC';
			break;

	}



	 $query = 'SELECT * FROM
		(SELECT #__gitags_tags.tag_id, GROUP_CONCAT(name) AS name, title, introtext AS text, hits, catid, sectionid,
		item_id, component, created,
		CONCAT(\'Matching tags: \', GROUP_CONCAT(name)) AS section,
		CONCAT(\'index.php?option=\', component, \'&view=article&id=\', item_id) AS href,
		\'2\' AS browsernav
		FROM #__gitags_items
		NATURAL JOIN #__gitags_tags
		JOIN #__content ON (#__gitags_items.item_id = #__content.id)
		WHERE state = 1 AND NOW() > publish_up
		GROUP BY #__gitags_items.item_id
		ORDER BY '.$order.') AS DerivedTable
		WHERE '.$where;

	$db->setQuery( $query, 0, $limit );
	$list = $db->loadObjectList();
	if(isset($list))
	{
		foreach($list as $key => $item)
		{
			$list[$key]->href = ContentHelperRoute::getArticleRoute($item->item_id, $item->catid, $item->sectionid);
		}
	}
	$rows = $list;
	return $rows;
    }
    
    function charLimit()
    {
       $plugin =& JPluginHelper::getPlugin('search', 'gitags');
       $pluginParams = new JParameter( $plugin->params );

        return (int)$pluginParams->get( 'char_count', 3 );
    }
    
     function useStopWords()
    {
       return array();
    }
}
?>