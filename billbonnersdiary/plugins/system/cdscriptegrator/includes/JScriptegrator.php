<?php
/**
 * Core Design Scriptegrator plugin for Joomla! 1.7
 * @author		Daniel Rataj, <info@greatjoomla.com>
 * @package		Joomla 
 * @subpackage	System
 * @category	Plugin
 * @version		2.1.4
 * @copyright	Copyright (C) 2007 - 2011 Great Joomla!, http://www.greatjoomla.com
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL 3
 * 
 * This file is part of Great Joomla! extension.   
 * This extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

defined('_JEXEC') or die;

jimport('joomla.filesystem.folder');

/**
 * JScripetgrator class
 * 
 * @author Daniel Rataj
 * 
 */
class JScriptegrator {
	
	public 			$plg_name 				= 	'cdscriptegrator';
	public 			$plg_version 			= 	'';
	public 			$path_to_folder 		= 	'/plugins/system/cdscriptegrator';
	
	/** Scriptegrator plugin parameters */
	private static	$plugin_params			=	array();
	
	/** Available libraries */
	private static	$available_libraries	= 	array();
	
	/** Errors in Scriptegrator plugin, like wrong version or missing JS libraries... */
	public 			$hasError				= 	array();
	
	/**
	 * Constructor
	 * 
	 * @return	void
	 */
	public function __construct() {
		self::$available_libraries = JFolder::folders($this->folderPath(true) . DS . 'libraries');
		
		// plugin version
		$xml = JFactory::getXML(dirname(dirname(__FILE__)) . DS . $this->plg_name . '.xml');
		
		// assign version
		$this->plg_version = (string) $xml->version->data();
		
		// assign plugin params
		self::$plugin_params = $this->scriptegratorPluginParams();
	}
	
	/**
	 * Get instance of JScriptegrator
	 * 
	 * @param	string		$id
	 * @param	string		$requirePluginVersion
	 * @return	instance
	 */
	public static function getInstance($requirePluginVersion = '', $id = '') {
		static $instances;
		
		if (!isset($instances)) {
			$instances = array();
		}
		
		$instancename = md5($requirePluginVersion . $id);
		
		if (empty($instances[$instancename])) {
			$JScriptegrator = new JScriptegrator();
			$instances[$instancename] = $JScriptegrator;
		}
		
		// check required version
		if ($requirePluginVersion and !$instances[$instancename]->versionRequire($requirePluginVersion)) {
			$instances[$instancename]->setError(JText::sprintf('PLG_SYSTEM_CDSCRIPTEGRATOR_ERROR_REQUIRE_VERSION', $requirePluginVersion));
		}
		
		return $instances[$instancename];
	}
	
	/**
	 * Library JS loader
	 * 
	 * @param	array	$libraries
	 * @return 	void
	 */
	public function importLibrary($libraries = array()) {
		
		// make sure is array, if not, convert it
		if (!is_array($libraries)) {
			$libraries = array($libraries);
		}
		
		$document = JFactory::getDocument();
		
		foreach($libraries as $library => $library_params) {
			// no parameters, so the library params is also the library name
			if (is_int($library)) {
				$library = $library_params;
				$library_params = array();
			}
			// check if library is allowed
			if ($library and !in_array($library, self::$available_libraries)) {
				$this->setError(JText::sprintf('PLG_SYSTEM_CDSCRIPTEGRATOR_ERROR_LIBRARY_DOES_NOT_EXISTS', $library));
				continue;
			}
			
			// define helper path
	        $library_class_path = $this->folderPath(true) . DS . 'libraries' . DS . $library . DS . $library . '.php';
	        
	        // load & register class file if exists
	        if (JFile::exists($library_class_path)) {
	        	JLoader::register($library , $library_class_path);
	        	
	        	$libraryInstance = call_user_func(array($library, 'getInstance'), $library_params);
	        	
	        	$files = array();
	        	
	        	// import header files
	        	if (is_callable(array($libraryInstance, 'importFiles'))) {
	        		$importFiles = call_user_func(array($libraryInstance, 'importFiles'));
	        		if (is_array($importFiles)) {
	        			$files = array_merge($files, array_values($importFiles));
	        		} else {
	        			// some error
	        			$this->setError($importFiles, $library);
	        			continue;
	        		}
	        		unset($importFiles);
	        	}
	        	
	        	// add script declaration, only ONCE
	        	if (is_callable(array($libraryInstance, 'scriptDeclaration'))) {
	        		static $addScriptDeclaration;
	        		
	        		if (!$addScriptDeclaration) {
		        		$script_declaration = call_user_func(array($libraryInstance, 'scriptDeclaration'));
		        		if ($script_declaration) {
		        			$document->addScriptDeclaration($script_declaration);
		        		}
		        		$addScriptDeclaration = true;
	        		}
	        	}
		        
				if ((int) self::$plugin_params->get('compression', 0)) {
					
					$js_files = array();
					$css_files = array();
					
					foreach($files as $file) {
						$extension = JFile::getExt($file);
						switch ($extension) {
							case 'js':
								$js_files []= $file;
								break;
							case 'css':
								$css_files []= $file;
								break;
							default: break;
						}
					}
					
					if ($js_files) $js_files = implode('&amp;', array_map(array($this, 'arrayToUrl'), $js_files));
					if ($css_files) $css_files = implode('&amp;', array_map(array($this, 'arrayToUrl'), $css_files));
					
					if (!is_array($js_files)) {
						$loader_filepath = $this->folderPath(true) . DS . 'libraries' . DS . $library . DS . 'gzip' . DS . 'js.php';
						
						if (JFile::exists($loader_filepath)) {
							$document->addScript($this->folderPath() . "/libraries/$library/gzip/js.php?" . $js_files);
						}
					}
					
					if (!is_array($css_files)) {
						$loader_filepath = $this->folderPath(true) . DS . 'libraries' . DS . $library . DS . 'gzip' . DS . 'css.php';
						
						if (JFile::exists($loader_filepath)) {
							$document->addStyleSheet($this->folderPath() . "/libraries/$library/gzip/css.php?" . $css_files);
						}
					}
					
				} else {
					if (is_array($files) and $files) {
						foreach ($files as $file) {
							$extension = JFile::getExt($file);
							$path = $this->folderPath() . "/libraries/$library/$file";
							
							switch ($extension) {
								case 'js':
									$document->addScript($path);
									break;
								case 'css':
									$document->addStyleSheet($path, 'text/css');
									break;
								default: break;
							}
						}
					}
				}
	        }
		}
	}
	
	/**
	 * Return Scriptegrator folder path
	 * 
	 * @param 	boolean	$absolute
	 * @return 	string
	 */
	public function folderPath($absolute = false) {
		$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
		$path = JURI::root(true) . $this->path_to_folder;
		if ($absolute) $path = JPath::clean($root . $this->path_to_folder);

		return $path;
	}

	/**
	 * Check version compatibility
	 * 
	 * @param	string	$min_version
	 * @return 	boolean
	 */
	public function versionRequire($min_version) {
		return (version_compare( $this->plg_version, $min_version, '>=' ) == 1);
	}
	
	/**
	 * Return list of available themes
	 * 
	 * @return array
	 */
	public function themeList() {
		jimport('joomla.filesystem.folder');
		$path = $this->folderPath(true) . DS . 'libraries' . DS . 'jqueryui' . DS . 'css';
		$files = array();
		$files = JFolder::folders($path, '.', false, false);
		return $files;
	}
	
	/**
	 * Get Scriptegrator plugin parameters
	 * 
	 * @param	string	$param
	 * @param	string	$default
	 * @return 	mixed	Array or string.
	 */
	public function scriptegratorPluginParams($param = '', $default = '') {
    	$plugin = JPluginHelper::getPlugin('system', $this->plg_name);
    	
    	jimport('joomla.registry.registry');
    	
        $params =  new JRegistry($plugin->params);
        
        // no defined param, just return all of them
        if (!$param) return $params;
        
        return $params->get($param, $default);
	}
	
	/**
     * Modify URL array
     * 
     * @param	string	$value
     * @param 	string	$name
     * @return 	array
     */
	private function arrayToUrl($value = '', $name = 'files') {
		return $name . '[]=' . $value;
	}
	
	/**
	 * Set error message
	 * 
	 * @param 	string		$error
	 * @return	boolean		True if error is set.
	 */
	private function setError($error = '') {
		if (is_string($error) and $error) {
			array_push($this->hasError, $error);
			return true;
		}
		return false;
	}
	
	/**
	 * Return JScriptegrator plugin
	 * 
	 * @return		string		Error message.
	 */
	public function getError() {
		if (is_array($this->hasError) and $this->hasError) {
			return implode("<br />", $this->hasError);
		}
		return '';
	}
}
?>