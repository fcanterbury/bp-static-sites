<?php
/**
* @package		Joomla
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
 * based on JTags   http://extensions.joomla.org/extensions/search-a-indexing/tags-a-clouds/5728 developed by Jacek Zielinski
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

/**
 * Joomla! System Insert Tags Plugin
 *
 * @author		Jacek Zielinski <jacek.zielinski@community.joomla.org>
 * @package		Joomla
 * @subpackage	System
 */
class plgSystemGiTags extends JPlugin
{
	/**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @access	protected
	 * @param	object	$subject The object to observe
	 * @param 	array   $config  An array that holds the plugin configuration
	 * @since	1.0
	 */
	function plgSystemGiTags(& $subject, $config)
	{
		parent::__construct($subject, $config);
	}

	/**
	* Injects Insert Tags input box and drop down menu to adminForm
	*
	* @access	public
	* @since	1.6
	*/
	function onAfterRender()
	{
		 $id = JRequest::getVar('id');
		 if (!$id)
		 {
		 		$cid = JRequest::getVar( 'cid' , array() , '' , 'array' );
				@$id = $cid[0];
				
				$view = JRequest::getVar('view');
				if ($view =='article') $path = '';
				else $path = '..'.DS;
		 }
		 $task = JRequest::getVar('task');
		 $option= JRequest::getVar('option');

		 $body = JResponse::getBody();
		 $tagsList = $this->getTags($id, $option);
		 $masterTagList = $this->getMasterTagsList(); 
                 $divList = $this->getDivList();
		 if ($option=='com_content')
		 {
			// Adding in the Javascript code needed to make the adding of tags easier.
			$header_code = '<script type="text/javascript">
                            function addTag() {
                                var tags = document.getElementById("tags");
                                var tag_list = document.getElementById("tag_list");
                                if (tags.value == "") {
                                        tags.value = tag_list.options[tag_list.selectedIndex].value;
                                } else if (tag_list.options[tag_list.selectedIndex].value == "--") {
                                } else {
                                    tags.value = tags.value + ", " + tag_list.options[tag_list.selectedIndex].value;
                                }
                            } 
                            
                               
                            function addTags()
                            {
                                
                                var tags = document.getElementById("tags");
                                $$(".cktag").each(
                                    function (el){
                                       if (el.checked)
                                       {
                                            val = tags.value;
                                            if (val.indexOf(el.value) == -1)
                                                if (tags.value == "")
                                                    tags.value = tags.value + el.value;
                                                else
                                                    tags.value = tags.value + ", " + el.value;
                                       }
                                })
                                 $$("#all_tags").toggle();
                                
                            }
                            
                            function showTagDiv(){
                                var tags = document.getElementById("tags").value;
                                arr = tags.split(",");
                                

                                $$(".cktag").each(
                                    function (el){
                                        for(i=0;i<arr.length;i++)
                                        {
                                            if (arr[i].trim() == el.value)
                                            {
                                                el.checked = true;
                                                break;
                                             }
                                         }
                                })
                                
                                $$("#all_tags").toggle();
                                        
                            }

                            
                                
                        </script>';
			$body = str_replace('<body', $header_code.'<body', $body);
			// Original Code:
		 	$body = str_replace('<div id="editor-xtd-buttons">', 'Insert tags separated by commas, or select from list: '.$masterTagList.'<input type="text" name="tags" id="tags" size="60" value="'.$tagsList.'"><br/>'.$divList.'<div id="editor-xtd-buttons">', $body);
		 }
		 JResponse::setBody($body);
	}

	/**
	* Retrieves tags list database
	*
	* @access	public
	* @param 	Int		$id		Item ID
	* @param 	String		$option		component
	* @return 	String		$tagsList	Tags list separated by commas
	* @since	1.6
	*/
	function getTags($id, $option)
	{
		$query = 'SELECT name FROM #__gitags_items NATURAL JOIN #__gitags_tags ' .
				 'WHERE component=\''.$option.'\' AND item_id=\''.$id.'\' order by `name` asc ';
		$db =& JFactory::getDBO();
		$db->setQuery($query);
		$tagsArray = $db->loadResultArray();
		$tagsList = implode(",", $tagsArray);
		return $tagsList;
	}

	
        /*
         * the drop down
         */
	function getMasterTagsList() {
		$query = 'SELECT `name` FROM #__gitags_tags ORDER BY `name` asc '; // This is the list of tags in the database.
		//Copy db code from function above.
		$db =& JFactory::getDBO();
		$db->setQuery($query);
		$tagsArray = $db->loadResultArray();
		$return = '<select name="tag_list" onchange="addTag()" id="tag_list">';
		$return .= '<option>--</option>';
		foreach($tagsArray as $value) {
			$return .= '<option value="'.$value.'">'.$value.'</option>';
		}
		$return .= '</select>';
                
		return $return;
	}
        
      /*
       * the list
       */  
     function getDivList()
     {
         
         $query = 'SELECT `name` FROM #__gitags_tags ORDER BY `name`'; // This is the list of tags in the database.
		//Copy db code from function above.
		$db =& JFactory::getDBO();
		$db->setQuery($query);
		$tagsArray = $db->loadResultArray();
		$return = '';
		
                $return .='<a href="javascript:void(0);" id="show_tag_div" onclick="showTagDiv();">open</a>';
                $return .= '<div id="all_tags" style="display:none"">';
                $i=0;
                foreach($tagsArray as $value) {
                    $return .= '<div style="float:left;position:relative; width:150px;"><input style="position:relative:float:left;margin:0px;padding:5px!important;" type="checkbox" id="tag_'.$i.'" value="'.$value.'"  class="cktag"/>&nbsp;&nbsp;'.$value.'</div>';
                    $i++;
                    if(($i%10) == 0)$return .= "<br/>";
                    if(($i%60) == 0)$return .= "<br clear='all'/><br/>";
		}
                $return .='<br/><input type="button" onclick="addTags()" value="Add"><br/>';
                $return .='</div>';
		return $return;
     }


}