<?php
/**
 * Tag Meta Community plugin for Joomla 1.6
 *
 * @author Luigi Balzano (info@sistemistica.it)
 * @package TagMeta
 * @copyright Copyright 2009 - 2011
 * @license GNU Public License
 * @link http://www.sistemistica.it
 * @version 1.6.0
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');
jimport('joomla.html.parameter');

define("WORD_COUNT_MASK", "/\p{L}[\p{L}\p{N}\p{Mn}\p{Pd}'\x{2019}]*/u");

class plgSystemTagMeta extends JPlugin
{
  /**
  *
  * @var boolean
  * @access  private
  */
  private $_clean_generator = false;

  public function __construct(& $subject, $config)
  {
    parent::__construct($subject, $config);
    $this->loadLanguage();
  }

  /**
   *
   * Build and return the (called) prefix (e.g. http://www.youdomain.com) from the current server variables
   *
   * We say 'called' 'cause we use HTTP_HOST (taken from client header) and not SERVER_NAME (taken from server config)
   *
   */
  static function getPrefix()
  {
    if (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) != 'off')) {
      $https = 's://';
    } else {
      $https = '://';
    }
    return 'http' . $https . $_SERVER['HTTP_HOST'];
  }

  /**
   *
   * Build and return the (called) base path for site (e.g. http://www.youdomain.com/path/to/site)
   *
   * @param  boolean  If true returns only the path part (e.g. /path/to/site)
   *
   */
  static function getBase($pathonly = false)
  {
    if (strpos(php_sapi_name(), 'cgi') !== false && !ini_get('cgi.fix_pathinfo') && !empty($_SERVER['REQUEST_URI'])) {
      // PHP-CGI on Apache with "cgi.fix_pathinfo = 0"

      // We use PHP_SELF
      if (!empty($_SERVER["PATH_INFO"])) {
        $p = strrpos($_SERVER["PHP_SELF"], $_SERVER["PATH_INFO"]);
        if ($p !== false) { $s = substr($_SERVER["PHP_SELF"], 0, $p); }
      } else {
        $p = $_SERVER["PHP_SELF"];
      }
      $base_path =  rtrim(dirname(str_replace(array('"', '<', '>', "'"), '', $p)), '/\\');
      // Check if base path was correctly detected, or use another method
      /*
         On some Apache servers (mainly using cgi-fcgi) it happens that the base path is not correctly detected.
         For URLs like http://www.site.com/index.php/content/view/123/5 the server returns a wrong PHP_SELF variable.

         WRONG:
         [REQUEST_URI] => /index.php/content/view/123/5
         [PHP_SELF] => /content/view/123/5

         CORRECT:
         [REQUEST_URI] => /index.php/content/view/123/5
         [PHP_SELF] => /index.php/content/view/123/5

         And this lead to a wrong result for JURI::base function.

         WRONG:
         JURI::base(true) => /content/view/123
         JURI::base(false) => http://www.site.com/content/view/123/

         CORRECT:
         getBase(true) =>
         getBase(false):http://www.site.com/
      */
      if (!empty($base_path)) { if (strpos($_SERVER['REQUEST_URI'], $base_path) !== 0) { $base_path = rtrim(dirname($_SERVER['SCRIPT_NAME']), '/\\'); } }
    } else {
      // We use SCRIPT_NAME
      $base_path = rtrim(dirname($_SERVER['SCRIPT_NAME']), '/\\');
    }

    return $pathonly === false ? self::getPrefix() . $base_path . '/' : $base_path;
  }

  /**
   *
   * Build and return the REQUEST_URI (e.g. /site/index.php?id=1&page=3)
   *
   */
  static function getRequestURI($redirect_mode = 0)
  {
    if ( ($redirect_mode === 1) && ( (isset($_SERVER['REDIRECT_URL'])) || (isset($_SERVER['HTTP_X_REWRITE_URL'])) ) ) {
      $uri = (isset($_SERVER['HTTP_X_REWRITE_URL'])) ? $_SERVER['HTTP_X_REWRITE_URL'] : $_SERVER['REDIRECT_URL'];
    } else {
      $uri = $_SERVER['REQUEST_URI'];
    }
    return $uri;
  }

  /**
   *
   * Build and return the (called) {siteurl} macro value
   *
   */
  static function getSiteURL()
  {
    $siteurl = str_replace( 'https://', '', self::getBase() );
    return rtrim(str_replace('http://', '', $siteurl), '/');
  }

  /**
   *
   * Build and return the (called) full URL (e.g. http://www.youdomain.com/site/index.php?id=12) from the current server variables
   *
   */
  static function getURL($redirect_mode = 0)
  {
    return self::getPrefix() . self::getRequestURI($redirect_mode);
  }

  /**
   *
   * Return the host name from the given address
   *
   * Reference http://www.php.net/manual/en/function.parse-url.php#93983
   *
   */
  static function getHost($address)
  {
    $parsedUrl = parse_url(trim($address));
    return @trim($parsedUrl['host'] ? $parsedUrl['host'] : array_shift(explode('/', $parsedUrl['path'], 2)));
  }

  /**
   *
   * Build list of keys in the form "key1|key2|...|keyN" for using into REGEXP
   *
   */
  static function buildMatchKeywords($keywords)
  {
    $keylist = array_map('trim', explode(",", $keywords));
    $matchkeys = '';
    foreach ($keylist as $k => $v)
    {
      $v = preg_quote($v); // Current key should be cleaned escaping chars for REGEXP
      if ($v != '') { $matchkeys .= $v . "|"; }
    }
    $matchkeys = rtrim($matchkeys, '|'); // Drop last char if is a pipe
    return $matchkeys;
  }

  /**
   *
   * Callback function used to replace macros in destination URL
   *
   */
  static function getMacroParamValue($macro, $params)
  {
    $value = '';
    switch ($macro)
    {
      case 'requestvar':
        // $params[0] =  variable name
        // $params[1] =  default value if variable is not set (optional)
        if ( isset($params[1]) ) {
          $value = JRequest::getVar($params[0], $params[1]);
        } else {
          $value = JRequest::getVar($params[0]);
        }
        break;
    }
    return $value;
  }

  public function onAfterDispatch()
  {
    $document =& JFactory::getDocument();
    $docType = $document->getType();

    // Get the application object
    $app = JFactory::getApplication();

    // Make sure we are not in the administrator
    if ( $app->isAdmin() ) return;

    // Only site pages that are html docs
    if ( JRequest::getBool('no_html') === true ) return;

    // Set siteurl and sitename macro values
    $siteurl = self::getSiteURL();
    $sitename = $app->getCfg('sitename');

    $currenturi_encoded = self::getRequestURI( $this->params->get('redirect', 0) ); // Raw (encoded): with %## chars
    // Remove the base path
    $basepath = trim($this->params->def('basepath', ''), ' /'); // Decoded: without %## chars (now you can see spaces, cyrillics, ...)
    $basepath = urlencode($basepath); // Raw (encoded): with %## chars
    if ($basepath != '')
    {
      if (strpos($currenturi_encoded, '/'.$basepath.'/') === 0)
      {
        $currenturi_encoded = substr($currenturi_encoded, strlen($basepath) + 1); // Raw (encoded): with %## chars
      }
    }
    $currenturi = urldecode($currenturi_encoded); // Decoded: without %## chars (now you can see spaces, cyrillics, ...)
    $currentfullurl_encoded = self::getPrefix() . $currenturi_encoded; // Raw (encoded): with %## chars
    $currentfullurl = urldecode($currentfullurl_encoded); // Decoded: without %## chars (now you can see spaces, cyrillics, ...)

    $db = JFactory::getDBO();
    $db->setQuery('SELECT * FROM #__tagmeta_rules '
    . 'WHERE ( '
    . '( (' . $db->quote($currenturi) . ' REGEXP BINARY url)>0 AND (case_sensitive<>0) AND (decode_url<>0) AND (request_only<>0) ) '
    . 'OR ( (' . $db->quote($currenturi_encoded) . ' REGEXP BINARY url)>0 AND (case_sensitive<>0) AND (decode_url=0) AND (request_only<>0) ) '
    . 'OR ( (' . $db->quote($currentfullurl) . ' REGEXP BINARY url)>0 AND (case_sensitive<>0) AND (decode_url<>0) AND (request_only=0) ) '
    . 'OR ( (' . $db->quote($currentfullurl_encoded) . ' REGEXP BINARY url)>0 AND (case_sensitive<>0) AND (decode_url=0) AND (request_only=0) ) '
    . 'OR ( (' . $db->quote($currenturi) . ' REGEXP url)>0 AND (case_sensitive=0) AND (decode_url<>0) AND (request_only<>0) ) '
    . 'OR ( (' . $db->quote($currenturi_encoded) . ' REGEXP url)>0 AND (case_sensitive=0) AND (decode_url=0) AND (request_only<>0) ) '
    . 'OR ( (' . $db->quote($currentfullurl) . ' REGEXP url)>0 AND (case_sensitive=0) AND (decode_url<>0) AND (request_only=0) ) '
    . 'OR ( (' . $db->quote($currentfullurl_encoded) . ' REGEXP url)>0 AND (case_sensitive=0) AND (decode_url=0) AND (request_only=0) ) '
    . ') '
    . 'AND published=1 '
    . 'ORDER BY ordering');
    $items = $db->loadObjectList();
    $itemsfound = count($items);
    if ($itemsfound > 0)
    {
        // Supported macros patterns
        $patterns = array();
        $patterns[0] = "/\{siteurl\}/";
        $patterns[1] = "/\{sitename\}/";
        $patterns[2] = "/\{globaldescription\}/";
        $patterns[3] = "/\{globalkeywords\}/";
        $patterns[4] = "/\{currenttitle\}/";
        $patterns[5] = "/\{currentdescription\}/";
        $patterns[6] = "/\{currentkeywords\}/";
        $patterns[7] = "/\{currentauthor\}/";
        $patterns[8] = "/\{currentgenerator\}/";
        $patterns[9] = "/\{requestvar ([^\}\,]+),?\}/e";
        $patterns[10] = "/\{requestvar ([^\}]+),([^\}]+)\}/e";

        // Supported macros replacements
        $replacements = array();
        $replacements[0] = $siteurl;
        $replacements[1] = $sitename;
        $replacements[2] = $app->getCfg('MetaDesc');
        $replacements[3] = $app->getCfg('MetaKeys');
        $replacements[4] = $document->getTitle();
        $replacements[5] = $document->getDescription();
        $replacements[6] = $document->getMetaData('keywords');
        $replacements[7] = $document->getMetaData('author');
        $replacements[8] = $document->getGenerator();
        $replacements[9] = "plgSystemTagMeta::getMacroParamValue('requestvar', array(0 => '\\1'))";
        $replacements[10] = "plgSystemTagMeta::getMacroParamValue('requestvar', array(0 => '\\1', 1 => '\\2'))";

        $currentitem = 0;
        $continue = true;
        while ($continue)
        {
            // Update hits
            $last_visit = date("Y-m-d H:i:s");
            $db->setQuery("UPDATE #__tagmeta_rules SET hits = hits + 1, last_visit = " . $db->quote( $last_visit ) . " WHERE id = " . $db->quote( $items[$currentitem]->id ));
            $res = @$db->query();

            // Replace macros
            $items[$currentitem]->title = preg_replace($patterns, $replacements, $items[$currentitem]->title);
            $items[$currentitem]->description = preg_replace($patterns, $replacements, $items[$currentitem]->description);
            $items[$currentitem]->author = preg_replace($patterns, $replacements, $items[$currentitem]->author);
            $items[$currentitem]->keywords = preg_replace($patterns, $replacements, $items[$currentitem]->keywords);
            $items[$currentitem]->canonical = str_replace( '{siteurl}', $siteurl, $items[$currentitem]->canonical ); // Only {siteurl} macro is supported for 'canonical'

            // Check for synonyms
            if (($items[$currentitem]->synonyms != 0) && ($items[$currentitem]->synonmax > 0)) {
              $binarymatch = ($items[$currentitem]->synonyms == 1) ? '' : 'BINARY'; // Add "BINARY" to "REGEXP" for case sensitive match
              $orderingmatch = ($items[$currentitem]->synonweight) ? 'weight DESC' : 'ordering ASC';
              $keywordsmatch = $this->buildMatchKeywords($items[$currentitem]->keywords);

              $db->setQuery("SELECT * FROM #__tagmeta_synonyms WHERE keywords REGEXP " . $binarymatch . " '(" . $keywordsmatch . ")' and published='1' ORDER BY " . $orderingmatch);
              $synonymsitems = $db->loadObjectList();
              if ( count($synonymsitems) > 0 ) {
                $keywords_list = $items[$currentitem]->keywords;
                if ($items[$currentitem]->synonyms == 1) { $keywords_list = strtolower($keywords_list); }
                $keywords_array = array_count_values(explode(",", $keywords_list)); // Not case sensitive: all to lowercase
                $keywordsmatch = '';
                $addedmatch = 0;
                $usedidmatch = array();
                foreach ($synonymsitems as $synonymskey => $synonymsvalue) {
                  if ($items[$currentitem]->synonyms == 1) { $synonymsvalue->synonyms = strtolower($synonymsvalue->synonyms); } // Not case sensitive: all to lowercase
                  $current_synonyms_list = array_count_values(array_map('trim', explode(",", $synonymsvalue->synonyms)));
                  foreach ($current_synonyms_list  as $current_synonyms_key => $current_synonyms_value) {
                    if ( ($current_synonyms_key != '') && (!isset($keywords_array[$current_synonyms_key])) ) {
                      // Add current keyword
                      $keywordsmatch .= $current_synonyms_key . ",";
                      $keywords_array[$current_synonyms_key] = isset($keywords_array[$current_synonyms_key]) ? $keywords_array[$current_synonyms_key] + 1 : 1;
                      $addedmatch++;
                      $usedidmatch[$synonymsvalue->id] = isset($usedidmatch[$synonymsvalue->id]) ? $usedidmatch[$synonymsvalue->id] + 1 : 1;
                      if ($addedmatch >= $items[$currentitem]->synonmax) { break; }
                    }
                  }
                  if ($addedmatch >= $items[$currentitem]->synonmax) { break; }
                }
                $keywordsmatch = rtrim($keywordsmatch, ','); // Drop last char if is a comma
                // Update keywords list
                $items[$currentitem]->keywords .= "," . $keywordsmatch;
                $items[$currentitem]->keywords = ltrim($items[$currentitem]->keywords, ','); // Drop first char if is a comma
                // Update hits on used synonyms items
                if ($addedmatch > 0) {
                  $usedidlist = '';
                  foreach ($usedidmatch as $usedidkey => $usedidvalue) {
                    $usedidlist .= $usedidkey . ",";
                  }
                  $usedidlist = rtrim($usedidlist, ','); // Drop last char if is a comma
                  $db->setQuery("UPDATE #__tagmeta_synonyms SET hits = hits + 1, last_visit = " . $db->quote( $last_visit ) . " WHERE id IN (" . $usedidlist . ")");
                  $res = @$db->query();
                }
              }
            }

            if ( !empty($items[$currentitem]->title) ) { $document->setTitle($items[$currentitem]->title); }
            if ( !empty($items[$currentitem]->description) ) { $document->setDescription(str_replace('"', '&quot;', $items[$currentitem]->description)); }
            if ( !empty($items[$currentitem]->author) ) { $document->setMetaData('author', $items[$currentitem]->author); }
            if ( !empty($items[$currentitem]->keywords) ) { $document->setMetaData('keywords', str_replace('"', '&quot;', $items[$currentitem]->keywords)); }
            if ( !empty($items[$currentitem]->canonical) ) { $document->addHeadLink($items[$currentitem]->canonical, 'canonical', 'rel'); }

            // Robots meta options: 0=No,1=Yes,2=Skip
            $robots = '';
            if ($items[$currentitem]->rindex != 2) { $robots .= ($items[$currentitem]->rindex) ? 'index,' : 'noindex,'; }
            if ($items[$currentitem]->rfollow != 2) { $robots .= ($items[$currentitem]->rfollow) ? 'follow,' : 'nofollow,'; }
            if ($items[$currentitem]->rsnippet != 2) { $robots .= ($items[$currentitem]->rsnippet) ? 'snippet,' : 'nosnippet,'; }
            if ($items[$currentitem]->rarchive != 2) { $robots .= ($items[$currentitem]->rarchive) ? 'archive,' : 'noarchive,'; }
            if ($items[$currentitem]->rodp != 2) { $robots .= ($items[$currentitem]->rodp) ? 'odp,' : 'noodp,'; }
            if ($items[$currentitem]->rimageindex != 2) { $robots .= ($items[$currentitem]->rimageindex) ? 'imageindex,' : 'noimageindex,'; }
            $robots = rtrim($robots, ','); // Drop last char if is a comma
            if ( !empty($robots) ) { $document->setMetaData('robots', $robots); }

            $last_rule = ($items[$currentitem]->last_rule);
            $currentitem++;
            $continue = ( (!$last_rule) && ($currentitem < $itemsfound) );
        }
    }

    $replacegenerator = $this->params->get('replacegenerator', 0);
    if ($replacegenerator != 0) {
      if ($replacegenerator == 3) {
        if ($document->getMetaData('generator')) {
          $document->setGenerator('');
          $this->_clean_generator = true; // Clean if exists
        }
      } else {
        $customgenerator = $this->params->get('customgenerator', '');
        if ( (($document->getMetaData('generator')) && ($replacegenerator == 1)) || ($replacegenerator == 2) ) {
          $document->setGenerator(str_replace('"', '&quot;', $customgenerator)); // Replace existing or force
        }
      }
    }

    $addsitename = $this->params->get('addsitename', 0);
    if ($addsitename != 0) {
      // Add site name before or after the page title
      $separator = str_replace( '\b', ' ', $this->params->get('separator', '\b-\b') );
      $currenttitle = $document->getTitle();
      if ( $addsitename == 1 ) {
        $newtitle = $sitename . $separator . $currenttitle; // Before
      } else {
        $newtitle = $currenttitle . $separator . $sitename; // After
      }
      $document->setTitle( htmlspecialchars_decode($newtitle) );
    }

    if ( $this->params->get('cleandefaultpage') == 1 ) {
      // Check if this is the default page (home page)
      $menu =& JSite::getMenu();
      if ( $menu->getActive() == $menu->getDefault() ) {
        $document->setTitle($sitename);
      }
    }

    $metatitle = $this->params->get('metatitle', 1);
    if ( $metatitle ) {
          $tagvalue = $document->getTitle();
          if ( empty($tagvalue) ) {
                // Tag title is empty
                $metavalue = $document->getMetaData('title');
                if ( ( !empty($metavalue) ) && ($metatitle == 2) ) {
                  $document->setTitle($metavalue);
                }
          } else {
                // Tag title is not empty
                $metavalue = $document->getMetaData('title');
                if ( ( !empty($metavalue) ) || ($metatitle == 2) ) {
                  $document->setMetaData('title', str_replace('"', '&quot;', $tagvalue));
                }
          }
    }

    $customauthor = $this->params->get('customauthor', '');
    $addauthor = $this->params->get('addauthor', 0);
    if ( ( $customauthor ) && ($addauthor != 0) ) {
          $currentauthor = $document->getMetaData('author');
          if ( ($addauthor == 1) || ( empty($currentauthor) ) ) {
                $document->setMetaData('author', str_replace('"', '&quot;', $customauthor));
          }
    }

    $customcopyright = $this->params->get('customcopyright', '');
    $addcopyright = $this->params->get('addcopyright', 0);
    if ( ( $customcopyright ) && ($addcopyright != 0) ) {
          $currentcopyright = $document->getMetaData('copyright');
          if ( ($addcopyright == 1) || ( empty($currentcopyright) ) ) {
                $document->setMetaData('copyright', str_replace('"', '&quot;', $customcopyright));
          }
    }

  }

  public function onAfterRender()
  {
    // Get the application object
    $app = JFactory::getApplication();

    // Make sure we are not in the administrator
    if ( $app->isAdmin() ) return;

    // Only site pages that are html docs
    if ( JRequest::getBool('no_html') === true ) return;

    $content = JResponse::getBody();
    $changed = false;

    // Clean meta tag generator
    if ($this->_clean_generator) {
      $content = preg_replace('/<meta.*name=[\",\']generator[\",\'].*\/?>/i', '', $content);
      $changed = true;
    }

    if ($changed) { JResponse::setBody($content); }
  }

}
