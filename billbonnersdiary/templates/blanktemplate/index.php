<?php

/**

 * @version                $Id: index.php 21518 2011-06-10 21:38:12Z chdemko $

 * @package                Joomla.Site

 * @subpackage  Templates.beez_20

 * @copyright        Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.

 * @license                GNU General Public License version 2 or later; see LICENSE.txt

 */



// No direct access.

defined('_JEXEC') or die;



// check modules

$showRightColumn        = ($this->countModules('position-3') or $this->countModules('position-6') or $this->countModules('position-8'));

$showbottom                        = ($this->countModules('position-9') or $this->countModules('position-10') or $this->countModules('position-11'));

$showleft                        = ($this->countModules('position-4') or $this->countModules('position-7') or $this->countModules('position-5'));



if ($showRightColumn==0 and $showleft==0) {

        $showno = 0;

}



JHtml::_('behavior.framework', true);



// get params

$color              = $this->params->get('templatecolor');

$logo               = $this->params->get('logo');

$navposition        = $this->params->get('navposition');

$app                = JFactory::getApplication();

$doc        = JFactory::getDocument();

$templateparams     = $app->getTemplate(true)->params;



$doc->addScript($this->baseurl.'/templates/blank_template/javascript/md_stylechanger.js', 'text/javascript', true);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >

        <head>
          <meta name="viewport" content="width=device-width, user-scalable=yes"  />
<script src="http://www.insiderstrategygroup.com/js/tfn_js_library_packed.js" type="text/javascript"></script>
          
<script>
    !window.jQuery && document.write('<script src="http://www.insideinvestingdaily.com/plugins/jquery.fancybox-1.3.4/jquery-1.4.3.min.js"><\/script>');
  </script>
          <script type="text/javascript" src="http://www.insideinvestingdaily.com/plugins/jquery.fancybox-1.3.4/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="http://www.insideinvestingdaily.com/plugins/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
          <link rel="stylesheet" type="text/css" href="http://www.insideinvestingdaily.com/plugins/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<script type="text/javascript">
    $(document).ready(function() {
           $("#about").fancybox({
        'height' :'50%',
        'width' :'40%',
        'autoScale'      : true,
        'type'        : 'iframe',
        'overlayColor'    : '#000',
        'padding' : 20,
        'overlayOpacity'  : 0.9
      });
         $("#contact").fancybox({
        'height' :'40%',
        'width' :'40%',
        'autoScale'      : true,
        'type'        : 'iframe',
        'overlayColor'    : '#000',
        'padding' : 20,
        'overlayOpacity'  : 0.9
      });
       $("#privates").fancybox({
        'height' :'70%',
        'width' :'60%',
        'autoScale'      : true,
        'type'        : 'iframe',
        'overlayColor'    : '#000',
        'padding' : 20,
        'overlayOpacity'  : 0.9
      });
          $("#faq").fancybox({
        'height' :'70%',
        'width' :'60%',
        'autoScale'      : true,
        'type'        : 'iframe',
        'overlayColor'    : '#000',
        'padding' : 20,
        'overlayOpacity'  : 0.9
      });
    });
  </script>
          <title>Inside Investing Daily</title>
          <script type="text/javascript">
function showPopup(url) {
newwindow=window.open(url,'name','height=600,width=800,top=200,left=300,scrollbars=1,resizable');
if (window.focus) {newwindow.focus()}
}
</script>

          
        </head>



        <body>



<div id="all">

        <div id="back">

             <div id="<?php echo $showRightColumn ? 'contentarea2' : 'contentarea'; ?>">


                                        <div id="breadcrumbs">



                                                        <jdoc:include type="modules" name="position-2" />



                                        </div>



                                        <?php if ($navposition=='left' AND $showleft) : ?>





                                                        <div class="left1 <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav">

                                                   <jdoc:include type="modules" name="position-7" style="beezDivision" headerLevel="3" />

                                                                <jdoc:include type="modules" name="position-4" style="beezHide" headerLevel="3" state="0 " />

                                                                <jdoc:include type="modules" name="position-5" style="beezTabs" headerLevel="2"  id="3" />





                                                        </div><!-- end navi -->

               <?php endif; ?>



                                        <div id="<?php echo $showRightColumn ? 'wrapper' : 'wrapper2'; ?>" <?php if (isset($showno)){echo 'class="shownocolumns"';}?>>



                                                <div id="main">



                                                <?php if ($this->countModules('position-12')): ?>

                                                        <div id="top"><jdoc:include type="modules" name="position-12"   />

                                                        </div>

                                                <?php endif; ?>



                                                        <jdoc:include type="message" />

                                                        <jdoc:include type="component" />



                                                </div><!-- end main -->



                                        </div><!-- end wrapper -->




                        <?php if ($navposition=='center' AND $showleft) : ?>



                                        <div class="left <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav" >



                                                <jdoc:include type="modules" name="position-7"  style="beezDivision" headerLevel="3" />

                                                <jdoc:include type="modules" name="position-4" style="beezHide" headerLevel="3" state="0 " />

                                                <jdoc:include type="modules" name="position-5" style="beezTabs" headerLevel="2"  id="3" />





                                        </div><!-- end navi -->

                   <?php endif; ?>



                                <div class="wrap"></div>



                                </div> <!-- end contentarea -->



                        </div><!-- back -->



                </div><!-- all -->



                <div id="footer-outer">

                        <?php if ($showbottom) : ?>

                        <div id="footer-inner">



                                <div id="bottom">

                                        <div class="box box1"> <jdoc:include type="modules" name="position-9" style="beezDivision" headerlevel="3" /></div>

                                        <div class="box box2"> <jdoc:include type="modules" name="position-10" style="beezDivision" headerlevel="3" /></div>

                                        <div class="box box3"> <jdoc:include type="modules" name="position-11" style="beezDivision" headerlevel="3" /></div>

                                </div>





                        </div>

                                <?php endif ; ?>



                        <div id="footer-sub">





                                <div id="footer">



                                        <jdoc:include type="modules" name="position-14" />

                                        <p>

                                                

                                        </p>





                                </div><!-- end footer -->



                        </div>



                </div>

        <jdoc:include type="modules" name="debug" />

        </body>

</html>

