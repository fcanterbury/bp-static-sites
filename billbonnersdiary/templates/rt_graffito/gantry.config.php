<?php
/**
 * @version   $Id: gantry.config.php 3718 2012-09-18 15:47:10Z kevin $
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
defined('JPATH_BASE') or die();

$gantry_presets = array(
    'presets' => array(
        'preset1' => array(
            'name' => 'Preset 1',
            'main-bg' => 'brown',
            'main-accent' => '#e3d84b'
        ),
        'preset2' => array(
            'name' => 'Preset 2',
            'main-bg' => 'blue',
            'main-accent' => '#7BCBDF'
        ),
        'preset3' => array(
            'name' => 'Preset 3',
            'main-bg' => 'red',
            'main-accent' => '#eb5e57'
        ),
        'preset4' => array(
            'name' => 'Preset 4',
            'main-bg' => 'green',
            'main-accent' => '#B7E37D'
        ),
        'preset5' => array(
            'name' => 'Preset 5',
            'main-bg' => 'purple',
            'main-accent' => '#C199FB'
        ),
        'preset6' => array(
            'name' => 'Preset 6',
            'main-bg' => 'gray',
            'main-accent' => '#b18963'
        ),
        'preset7' => array(
            'name' => 'Preset 7',
            'main-bg' => 'purple',
            'main-accent' => '#FF9F43'
        ),
        'preset8' => array(
            'name' => 'Preset 8',
            'main-bg' => 'light-brown',
            'main-accent' => '#93EBDD'
        )
    )
);
