<?php
/**
* @version   $Id: index.php 4022 2012-10-01 15:33:24Z kevin $
 * @author RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2012 RocketTheme, LLC
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 * Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
 *
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted index access' );

// load and inititialize gantry class
require_once('lib/gantry/gantry.php');
/** @var $gantry Gantry */
$gantry->init();

?>
<!doctype html>
<html xml:lang="<?php echo $gantry->language; ?>" lang="<?php echo $gantry->language;?>" >
<head>
  <meta name="google-site-verification" content="BKtGngkVeqFi7vPM_hrwDeALSQITrCNvNv5iZ4n-Pkc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
        $gantry->displayHead();

    $gantry->addStyle('grid-responsive.css', 5);
        $gantry->addLess('global.less', 'master.css', 8, array('main-accent'=>$gantry->get('main-accent', '#FFEB54')));

        if ($gantry->browser->name == 'ie'){
      if ($gantry->browser->shortversion == 8){
        $gantry->addScript('html5shim.js');
      }
    }

        $gantry->addScript('rokmediaqueries.js');
    if ($gantry->get('loadtransition')) {  
    $gantry->addScript('load-transition.js');
    $hidden = ' class="rt-hidden"';}

    ?>
</head>
<body <?php echo $gantry->displayBodyTag(); ?>>
  <div class="rt-bg">
    <?php /** Begin Drawer **/ if ($gantry->countModules('drawer')) : ?>
    <div id="rt-drawer">
        <div class="rt-container">
            <?php echo $gantry->displayModules('drawer','standard','standard'); ?>
            <div class="clear"></div>
        </div>
    </div>
    <?php /** End Drawer **/ endif; ?>
    <?php /** Begin Top Surround **/ if ($gantry->countModules('top') or $gantry->countModules('header')) : ?>
    <header id="rt-top-surround">
    <?php /** Begin Top **/ if ($gantry->countModules('top')) : ?>
    <div id="rt-top" <?php echo $gantry->displayClassesByTag('rt-top'); ?>>
      <div class="rt-container">
        <?php echo $gantry->displayModules('top','standard','standard'); ?>
        <div class="clear"></div>
      </div>
    </div>
    <?php /** End Top **/ endif; ?>
    <?php /** Begin Header **/ if ($gantry->countModules('header')) : ?>
    <div id="rt-header">
      <div class="rt-container">
        <?php echo $gantry->displayModules('header','standard','standard'); ?>
        <div class="clear"></div>
      </div>
    </div>
    <?php /** End Header **/ endif; ?>
  </header>
  <?php /** End Top Surround **/ endif; ?>
  <?php /** Begin Showcase **/ if ($gantry->countModules('showcase')) : ?>
  <div id="rt-showcase">
    <div class="rt-showcase-pattern">
      <div class="rt-container">
        <?php echo $gantry->displayModules('showcase','standard','standard'); ?>
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <?php /** End Showcase **/ endif; ?>
  <div id="rt-transition"<?php if ($gantry->get('loadtransition')) echo $hidden; ?>>
    <div class="rt-container main-surround">
      <div id="rt-mainbody-surround">
        <?php /** Begin Feature **/ if ($gantry->countModules('feature')) : ?>
        <div id="rt-feature">
          <div class="rt-container">
            <?php echo $gantry->displayModules('feature','standard','standard'); ?>
            <div class="clear"></div>
          </div>
        </div>
        <?php /** End Feature **/ endif; ?>
        <?php /** Begin Utility **/ if ($gantry->countModules('utility')) : ?>
        <div id="rt-utility">
          <div class="rt-container">
            <?php echo $gantry->displayModules('utility','standard','standard'); ?>
            <div class="clear"></div>
          </div>
        </div>
        <?php /** End Utility **/ endif; ?>
        <?php /** Begin Breadcrumbs **/ if ($gantry->countModules('breadcrumb')) : ?>
        <div id="rt-breadcrumbs">
          <div class="rt-container">
            <?php echo $gantry->displayModules('breadcrumb','standard','standard'); ?>
            <div class="clear"></div>
          </div>
        </div>
        <?php /** End Breadcrumbs **/ endif; ?>
        <?php /** Begin Main Top **/ if ($gantry->countModules('maintop')) : ?>
        <div id="rt-maintop">
          <div class="rt-container">
            <?php echo $gantry->displayModules('maintop','standard','standard'); ?>
            <div class="clear"></div>
          </div>
        </div>
        <?php /** End Main Top **/ endif; ?>
        <?php /** Begin Main Body **/ ?>
            <?php echo $gantry->displayMainbody('mainbody','sidebar','standard','standard','standard','standard','standard'); ?>
        <?php /** End Main Body **/ ?>
        <?php /** Begin Main Bottom **/ if ($gantry->countModules('mainbottom')) : ?>
        <div id="rt-mainbottom">
          <div class="rt-container">
            <?php echo $gantry->displayModules('mainbottom','standard','standard'); ?>
            <div class="clear"></div>
          </div>
        </div>
        <?php /** End Main Bottom **/ endif; ?>
        <?php /** Begin Extension **/ if ($gantry->countModules('extension')) : ?>
        <div id="rt-extension">
          <div class="rt-container">
            <?php echo $gantry->displayModules('extension','standard','standard'); ?>
            <div class="clear"></div>
          </div>
        </div>
        <?php /** End Extension **/ endif; ?>
      </div>
    </div>
  </div>
  <?php /** Begin Bottom **/ if ($gantry->countModules('bottom')) : ?>
  <div id="rt-bottom">
    <div class="rt-container">
      <?php echo $gantry->displayModules('bottom','standard','standard'); ?>
      <div class="clear"></div>
    </div>
  </div>
  <?php /** End Bottom **/ endif; ?>
  <?php /** Begin Footer Section **/ if ($gantry->countModules('footer') or $gantry->countModules('copyright')) : ?>
  <footer id="rt-footer-surround">
    <?php /** Begin Footer **/ if ($gantry->countModules('footer')) : ?>
    <div id="rt-footer">
      <div class="rt-container">
        <?php echo $gantry->displayModules('footer','standard','standard'); ?>
        <div class="clear"></div>
      </div>
    </div>
    <?php /** End Footer **/ endif; ?>
    <?php /** Begin Copyright **/ if ($gantry->countModules('copyright')) : ?>
    <div id="rt-copyright">
      <div class="rt-container">
        <?php echo $gantry->displayModules('copyright','standard','standard'); ?>
        <div class="clear"></div>
      </div>
    </div>
    <?php /** End Copyright **/ endif; ?>
  </footer>
  <?php /** End Footer Surround **/ endif; ?>
</div>
  <?php /** Begin Debug **/ if ($gantry->countModules('debug')) : ?>
  <div id="rt-debug">
    <div class="rt-container">
      <?php echo $gantry->displayModules('debug','standard','standard'); ?>
      <div class="clear"></div>
    </div>
  </div>
  <?php /** End Debug **/ endif; ?>
  <?php /** Begin Auxiliary **/ if ($gantry->countModules('auxiliary') and ($gantry->countModules('sidepanel'))) : ?>
  <?php echo $gantry->displayModules('auxiliary','basic','basic'); ?>
  <?php /** End Auxiliary **/ endif; ?>
  <?php /** Begin Popups **/
      echo $gantry->displayModules('popup','popup','popup');
      echo $gantry->displayModules('login','login','popup');
      /** End Popup s**/ ?>
  <?php /** Begin Analytics **/ if ($gantry->countModules('analytics')) : ?>
  <?php echo $gantry->displayModules('analytics','basic','basic'); ?>
  <?php /** End Analytics **/ endif; ?>
  </body>
</html>
<?php
$gantry->finalize();
?>
