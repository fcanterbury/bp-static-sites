((function(){this.StylePanel=new Class({Implements:[Events,Options],options:{status:"hide",maxWidth:2000},initialize:function(e,a,f){this.setOptions(f);
this.element=document.getElement(e)||document.id(e)||null;this.toggler=document.getElement(a)||document.id(a)||null;if(!e){throw new Error('Side Panel "'+e+'" not found in the DOM.');
}if(!a){throw new Error('Toggle "'+a+'" for Style Panel not fond in the DOM.');}this.bounds={resize:this.resizeCheck.bind(this)};this.size=this.element.getSize().x;
if(window.getSize().x<=this.options.maxWidth){this.options.status="hide";}this.fx=new Fx.Tween(this.element,{duration:350,transition:"quad:in:out",onComplete:this.onComplete.bind(this,this.element)});
this[this.options.status](null,"silent");this.onComplete(this.element);this.attach();},attach:function(){this.toggler.addEvent("click",this.toggle.bind(this));
window.addEvent("resize",this.bounds.resize);},detach:function(){this.toggler.removeEvents();window.removeEvent("resize",this.bounds.resize);},show:function(d,a){this.fx[a?"set":"start"]("left",0);
},hide:function(d,a){Object.each(window.moorainbow,function(c,f){if(c.visible){c.hide();}});this.fx[a?"set":"start"]("left",-this.size);},toggle:function(f,a){var e=this.element.getStyle("left").toInt();
this[!e?"hide":"show"](f,a);},onComplete:function(d){var a=d.getStyle("left").toInt();this.toggler.removeClass("pig").removeClass("turkey");this.toggler.addClass(!a?"pig":"turkey");
},resizeCheck:function(){if(window.getSize().x<=this.options.maxWidth&&this.element.getStyle("left").toInt()===0){this.hide();}if(window.getSize().x>this.options.maxWidth&&this.element.getStyle("left").toInt()<0){this.show();
}}});var b=new Class({Implements:[Options,Events],options:{container:window,max:0,min:0,mode:"vertical"},initialize:function(d){this.setOptions(d);this.container=document.id(this.options.container);
this.enters=this.leaves=0;this.inside=false;var a=this;this.listener=function(c){var h=a.container.getScroll(),e=h[a.options.mode=="vertical"?"y":"x"];
if(e>=a.options.min&&(a.options.max===0||e<=a.options.max)){if(!a.inside){a.inside=true;a.enters++;a.fireEvent("enter",[h,a.enters,c]);}a.fireEvent("tick",[h,a.inside,a.enters,a.leaves,c]);
}else{if(a.inside){a.inside=false;a.leaves++;a.fireEvent("leave",[h,a.leaves,c]);}}a.fireEvent("scroll",[h,a.inside,a.enters,a.leaves,c]);};this.addListener();
},start:function(){this.container.addEvent("scroll",this.listener);},stop:function(){this.container.removeEvent("scroll",this.listener);},addListener:function(){this.start();
}});window.addEvent("domready",function(){var a=document.getElement(".side-panel-container"),d=new StylePanel(".side-panel-container",".side-panel-toggle",{status:"show"});
ss=new b({min:1,onEnter:function(){a.setStyle("position","fixed");},onLeave:function(){a.setStyle("position","absolute");}});window.fireEvent("scroll");
});})());