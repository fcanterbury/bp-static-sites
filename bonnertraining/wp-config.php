<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_bonnertrain');

/** MySQL database username */
define('DB_USER', 'wordpress_5');

/** MySQL database password */
define('DB_PASSWORD', 'B$888part');

/** MySQL hostname */
define('DB_HOST', 'localhost:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '73VbQ6YvgOxw4@wZ2lT5!&tN7MnaAs5l^XV6LrV8qvcbnNmGM^tvpbol&kwMGODv');
define('SECURE_AUTH_KEY',  'xEju47B4lYPe8C8d@BWV*LxUiAYdbgGhS4^CKXTG3QsfMFR)ikI(R!IlwywfoWkn');
define('LOGGED_IN_KEY',    '4eiNcGeFJ3kqtkh&BL&!4)a#wtM6P4Zp3g*5OSBtC11T7I7xbEptGpEkuj198&fZ');
define('NONCE_KEY',        'QPNYUuT79Ln7GwoffdjqKta9ej4mUzfj6TrEQMotU*IADyDg1s0PtKmalGp9XqKZ');
define('AUTH_SALT',        'MDqPjm8Odq^f8eMC9SdeAHzqdZ16@orU5QC7E8gveR1maS6&*PbhgzqAIpsT92#3');
define('SECURE_AUTH_SALT', 'Qt9JIi3L!aZir7bZU5DUIyQY#yO!XTAXHgN5ZXTiLI@8vXYFCcdqt6RbIuw1gaw&');
define('LOGGED_IN_SALT',   'iS9wY1g97g*gTFbg6kygkmwEQIo14!PHOoEd5tJT3lEP@xe2mGZ3k7N9ZgGEYnGH');
define('NONCE_SALT',       'Iw^tCqKd45hX3ZC4sRdUnJDiVkj14ADh!jWB*Bu!INUa9kzI0Aybb@Gdi1eTXEu!');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'T21GYvin6_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'WP_ALLOW_MULTISITE', true );

define ('FS_METHOD', 'direct');

//--- disable auto upgrade
define( 'AUTOMATIC_UPDATER_DISABLED', true );
?>
