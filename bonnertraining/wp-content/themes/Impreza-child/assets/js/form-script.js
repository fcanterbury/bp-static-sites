jQuery(document).ready(function($){
    $(function() {
        $('form.leadgen-ajax').submit(function(event) {
            var form = $(this);
            $.ajax({
                async: true,
                type: form.attr('method'),
                url: '/wp-content/themes/Impreza-child/forms.php',
                data: form.serialize(),
                success: function(res){
                    window.location = "http://bonnertraining.com/exponential-technology-investing-whatscoming-and-where-to-profit/";
                },
                fail: function(){
                    $('.result-holder').text("Error");
                }
            });
            event.preventDefault(); // Prevent the form from submitting via the browser.
        });
    }); /* end ajax call */
});