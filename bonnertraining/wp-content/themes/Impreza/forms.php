<?php
header('Content-Type: application/json');

$return = array();
$error = false;
$resStr= "";
$resArr =  array();
$buildString = '?';
foreach ($_POST as $param_name => $param_val) {
    $arrPostVars[$param_name] = $param_val;
}

if(!empty($arrPostVars))
{

    if(filter_var($arrPostVars['email'],FILTER_VALIDATE_EMAIL)){

       // file_get_contents('http://signup.bonnerandpartners.com/Content/SaveFreeSignups'.$buildString);
        // Get cURL resource
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://signup.bonnerandpartners.com/Content/SaveFreeSignups',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $arrPostVars,
            CURLOPT_FOLLOWLOCATION => true
        ));
        // Send the request & save response to $resp

        $resultFromCurl = curl_exec($curl);
        if (!$resultFromCurl) {

            $error = true;
            array_push($resArr, 'Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));

        }
        else
        {
            array_push($resArr, 'Thank you');
        }
        // Close request to clear up some resources
        curl_close($curl);

    }
    else
    {
        $error = true;
        array_push($resArr, 'Not a valid email');
    }
}
else
{
    $error = true;
    array_push($resArr, 'No post vars');
}

if($error){$resStr= implode(",", $resArr);}else{$resStr="Thank you";}



$return['error'] = $error;
$return['resStr'] = $resStr;
$return['message'] = $resArr;

echo json_encode($return);