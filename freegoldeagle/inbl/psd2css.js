//
// psd2css.js
//
//   This is where all the javascript required by your design is written.
//
//   Originally generated at http://psd2cssonline.com 
//   January 6, 2014, 7:24 pm with psd2css Online version 1.95

$(document).ready(function(){

  // This is required for the PNG fix to work.
  if (window.DD_belatedPNG)
    DD_belatedPNG.fix('.pngimg');

  // This is some javascript to stop IE from displaying the img alt attributes
  // when you mouse over imagess.  If you would like IE to display the alt attributes,
  // just comment out the following 4 lines.  Don't worry, if you leave this in 
  // place, your ALT attributes are still readable by the search engines.
  var tmpalt;
  $("img").hover( 
    function(){ tmpalt = $(this).attr( "alt" ); $(this).attr( "alt", "" ); },
    function(){ $(this).attr( "alt", tmpalt ); });


  // Here are the functions to support communication between your
  // video controls and your video(s).  The video(s) play in p2cvideo.swf.
  // That swf file has registered the p2cplay(), p2crewind() and p2cpause() callbacks
  // with the javascript instance on this page.

  function getFlashVideo(div) {
    var flashVideo = ($('#'+div+' [name="'+div+'-video"]')[0]) ?
                      $('#'+div+' [name="'+div+'-video"]')[0] :
                            ((window[div+'-video']) ?
                              window[div+'-video'] : document[div+'-video']);
    return flashVideo;
  }




























































});
