<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'invnews_db');

/** MySQL database username */
define('DB_USER', 'invnews_user');

/** MySQL database password */
define('DB_PASSWORD', 'srDVYAxdVG2c8am');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N}<6@8xa3Qvds:SN jP/ye7[|LjDt XZzZP1V~)*hlVpUf-pu?EuZ#iaH<?|{JW]');
define('SECURE_AUTH_KEY',  '<BOXr@CpDFO*CSNex({nO[<EMk{5_)~J5-=$-|e+3N),B(xO~`+_JcEg(j_Gvc^h');
define('LOGGED_IN_KEY',    '44gs;JWShjN[iv6aS3C+AX)$u`63gWcz_rAJ<PXa!Rs(.gN,cua->gk1BtRhS-2)');
define('NONCE_KEY',        'O^Bjl-Tq|:oU6m;u~O|nq{t+)qE6QMC=Ep%FPD`|Uh}Kb1&$m|A 5L^StauxCYfJ');
define('AUTH_SALT',        'hNnl%kAn%i0Q)o+u(Jb]8XRv-)kTKBfrR|)o#Q!!Tcd*wzc!_{}A?#m?ASFBE3[5');
define('SECURE_AUTH_SALT', 'QHBYbJyLf[v%x*@%GS<SD<I4lsg!&-/(lG6qmp|D#CoNhbDC{yz4x8VZW8ZRc{cN');
define('LOGGED_IN_SALT',   ')gz-=T-b_`q]{AwhU[WiS+6))3)kt?3QH9(*R|Kar&Yq__1z|KE2sNg(xP5zgjg ');
define('NONCE_SALT',       ';FE|^|/bV5M|Je}-B(+X;OIo3?![!,lIp^nl1`w::++6_wD|NM@/O56`oxBJ1fuc');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
