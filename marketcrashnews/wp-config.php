<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mktcrash_db');

/** MySQL database username */
define('DB_USER', 'mktcrash_user');

/** MySQL database password */
define('DB_PASSWORD', 'srDVYAxdVG2c8am');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{W)*sjO 1?H`;ZM)4NBACht*EF+2.i<^s8l;x )%s^m2y+${#+X_,[ 2H-8p|&?w');
define('SECURE_AUTH_KEY',  'M7ze|-j)f-vjMz+X6YP+QHsm[v U+>k2Z&qWOi;2ukQLW#tjg[b|y%:%+-+ :w[3');
define('LOGGED_IN_KEY',    '-n1yfuTZ_}NnTlbJ964~@{#+ifc~+O2h[~#$P`k6AwiY=#juu*X09)06I!|9E>Ny');
define('NONCE_KEY',        '0{6XR>j*;il@8[h3bAdOLCiW|#HxV#+ P@^t-b9dXtCrW[O6z-$0_$0tbkiB:OQ*');
define('AUTH_SALT',        'P5D],2@8-pX)aDOB/O==Lr0I)oo7fN}s(`)qfiym:0De(!Z#j(~ ~*k7orJ%Lw;;');
define('SECURE_AUTH_SALT', 'T=,f|@xMUFn(XjGTus#L0BEn[!DudOpDF-n1+R:tZyYPMFi7OjaX#|bq|6rLlzpo');
define('LOGGED_IN_SALT',   '6T0`?r+YS:xFMlhQpe5KGAmqIvI*S/.HV~o_md:#bxb=S>tPxwT4+vR3Q[VoykkW');
define('NONCE_SALT',       'ST:|VvC4pz@mtT7WF^husfWk%9{9cNk1[7/o&%ET3y*(pz32zy~|0~K0KR;P3CZ^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
